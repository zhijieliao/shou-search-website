import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mms-information-dialog',
  templateUrl: './information-dialog.component.html',
  styleUrls: ['./information-dialog.component.css']
})
export class InformationDialogComponent implements OnInit {
  @Input() title = '提示';
  @Input() text = '';
  @Output() close = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

}
