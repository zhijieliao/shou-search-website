import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtifsSearchResultComponent } from './extifs-search-result.component';

describe('ExtifsSearchResultComponent', () => {
  let component: ExtifsSearchResultComponent;
  let fixture: ComponentFixture<ExtifsSearchResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtifsSearchResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtifsSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
