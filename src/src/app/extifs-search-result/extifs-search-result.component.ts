import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { HttpService } from '../service/http.service';

@Component({
  selector: 'mms-extifs-search-result',
  templateUrl: './extifs-search-result.component.html',
  styleUrls: ['./extifs-search-result.component.css']
})
export class ExtifsSearchResultComponent implements OnInit, OnDestroy {
  // /extifs/search-result?account=account&username=username&secretKey=secretKey
  sub: Subscription;

  account: string;
  username: string;
  secretKey: string;

  constructor(public http: HttpService, public route: ActivatedRoute) {
    this.http.isExtifsMode = true;
    this.http.authInfo.isAuthenticated = false;
    this.http.authInfo.account = null;
    this.http.authInfo.username = null;
  }

  ngOnInit() {
    this.sub = this.route.queryParamMap.subscribe(
      (params: ParamMap) => {
        if (params.has('account') && params.has('username') && params.has('secretKey')) {
          this.account = params.get('account');
          this.username = params.get('username');
          this.secretKey = params.get('secretKey');

          if ((this.account.length > 0) && (this.username.length > 0) && (this.secretKey.length > 0)) {
            if (this.checkSecretKey()) {
              this.http.authInfo.isAuthenticated = true;
              this.http.authInfo.account = this.account;
              this.http.authInfo.username = this.username;
              this.http.authInfo.secretKey = this.secretKey;
            }
          }
        }
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  checkSecretKey(): boolean {
    const secretKey = this.http.extifsEncrypt(
        '{' +
          '"/extifs/search":{' +
            '"user":{' +
              '"account":"' + this.account.toUpperCase() + '",' +
              '"username":"' + this.username.toUpperCase() + '"' +
            '}' +
          '}' +
        '}'
      );
    if (this.secretKey === secretKey) {
      return true;
    }
    return false;
  }

}
