import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mms-video-tags-management-more-tags',
  templateUrl: './video-tags-management-more-tags.component.html',
  styleUrls: ['./video-tags-management-more-tags.component.css']
})
export class VideoTagsManagementMoreTagsComponent implements OnInit {
  @Input() mediaName = '';
  @Input() tags = '';
  // moreTags: Array<Array<string>> = [];

  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  constructor() {
  }

  ngOnInit() {
    // const moreTagArray = this.tags.split(',');
    // let row = 0;
    // for (let i = 0; i < moreTagArray.length; ++i) {
    //   if ( (i % 5) === 0) {
    //     this.moreTags[row++] = new Array<string>();
    //   }

    //   this.moreTags[row - 1].push(moreTagArray[i]);
    // }
  }
}
