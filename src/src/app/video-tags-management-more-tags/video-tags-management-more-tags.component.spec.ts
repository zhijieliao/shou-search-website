import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoTagsManagementMoreTagsComponent } from './video-tags-management-more-tags.component';

describe('VideoTagsManagementMoreTagsComponent', () => {
  let component: VideoTagsManagementMoreTagsComponent;
  let fixture: ComponentFixture<VideoTagsManagementMoreTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoTagsManagementMoreTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoTagsManagementMoreTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
