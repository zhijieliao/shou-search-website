import { Directive, ElementRef, Input, HostListener } from '@angular/core';
import { Validator } from '../common/validator';

declare const $: JQueryStatic;

@Directive({
  selector: 'input[mms-validator],textarea[mms-validator]'
})
export class ValidatorDirective {
  _validator: Validator;

  @Input()
  set validator(validator) {
    this._validator = validator;
    if (validator) {
      validator.result = (success: boolean, message: string, focus: boolean) => {
        const $ele = $(this.ele.nativeElement);
        if (!success) {
          $ele.attr('title', message).css('border', '1px solid red');
        } else {
          $ele.removeAttr('title').css('border', '1px solid #DCDCDC');
        }

        if (focus) {
          $ele.focus();
        }
      };
    }
  }
  get validator() {
    return this._validator;
  }

  constructor(private ele: ElementRef) {
    $(this.ele.nativeElement).css('border', '1px solid #DCDCDC');
  }

  @HostListener('blur') onblur() {
    if (this._validator) {
      this._validator.validate(false);
    }
  }
}
