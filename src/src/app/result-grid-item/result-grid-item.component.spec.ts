import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultGridItemComponent } from './result-grid-item.component';

describe('ResultGridItemComponent', () => {
  let component: ResultGridItemComponent;
  let fixture: ComponentFixture<ResultGridItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultGridItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultGridItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
