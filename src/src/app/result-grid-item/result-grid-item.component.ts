import { Component, OnInit, Input } from '@angular/core';
import { MediaSearch } from '../common/media-search-reply';
import { HttpService } from '../service/http.service';
import { mediaIsVideo } from '../common/media-is-video';

@Component({
  selector: 'mms-result-grid-item',
  templateUrl: './result-grid-item.component.html',
  styleUrls: ['./result-grid-item.component.css']
})
export class ResultGridItemComponent implements OnInit {
  @Input() courseResList: MediaSearch.Course[];
  @Input() resourceResList: MediaSearch.Resource[];

  constructor(public http: HttpService) { }

  ngOnInit() {
  }

  isVideo(mediaType: string): boolean {
    return mediaIsVideo(mediaType);
  }
  hasSecond(second: number): boolean {
    if (second || (second === 0)) {
      return true;
    }
    return false;
  }

  gotoCourseDetail(course: MediaSearch.Course) {
    if (course.mediaUrlId) {
      if (this.isVideo(course.mediaType) && this.hasSecond(course.mediaSecond)) {
        window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?courseId=' + course.courseUrlId + '&resId=' + course.mediaUrlId + '&second=' + course.mediaSecond);
      } else {
        window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?courseId=' + course.courseUrlId + '&resId=' + course.mediaUrlId);
      }
    } else {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?courseId=' + course.courseUrlId);
    }
  }

  gotoResourceDetail(resource: MediaSearch.Resource) {
    if (this.isVideo(resource.mediaType) && this.hasSecond(resource.mediaSecond)) {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + resource.mediaUrlId + '&second=' + resource.mediaSecond);
    } else {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + resource.mediaUrlId);
    }
  }
}
