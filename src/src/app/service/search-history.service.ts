import { Injectable } from '@angular/core';

@Injectable()
export class SearchHistoryService {
  videoTag: {
    mediaName: string;
    selectedTags: string;
    beginTime: string;
    endTime: string;
    framesTagged: string;
    currentPage: number;
  };

  videoHistoryTags: string[] = [];

  frameTag: {
    mediaId: string;
    selectedTags: string;
    beginTime: string;
    endTime: string;
  };

  frameHistoryTags: string[] = [];

  constructor() {
    this.loadVideoTagCondtion();
    this.loadFrameTagCondtion();
    this.loadVideoHistoryTags();
    this.loadFrameHistoryTags();
  }

  saveVideoTagCondition() {
    sessionStorage.setItem('videoTagCondition', JSON.stringify(this.videoTag));
  }

  loadVideoTagCondtion() {
    const condition = sessionStorage.getItem('videoTagCondition');
    if (condition) {
      this.videoTag = JSON.parse(condition);
      if (this.videoTag) {
        return;
      }
    }
    this.videoTag = {
      mediaName: '',
      selectedTags: '',
      beginTime: '',
      endTime: '',
      framesTagged: '0',
      currentPage: 1
    };
  }

  saveFrameTagCondition() {
    sessionStorage.setItem('frameTagCondition', JSON.stringify(this.frameTag));
  }

  loadFrameTagCondtion() {
    const condition = sessionStorage.getItem('frameTagCondition');
    if (condition) {
      this.frameTag = JSON.parse(condition);
      if (this.frameTag) {
        return;
      }
    }
    this.frameTag = {
      mediaId: '',
      selectedTags: '',
      beginTime: '',
      endTime: '',
    };
  }

  updateVideoHistoryTags(newTags: string[]) {
    for (const tag of newTags) {
      if (tag.length > 0 && this.videoHistoryTags.indexOf(tag) === -1) {
        this.videoHistoryTags.unshift(tag);
        if (this.videoHistoryTags.length > 20) {
          this.videoHistoryTags.pop();
        }
      }
    }
    localStorage.setItem('videoHistoryTags', JSON.stringify(this.videoHistoryTags));
  }
  getVideoHistoryTags(): string[] {
    return this.videoHistoryTags;
  }
  loadVideoHistoryTags() {
    this.videoHistoryTags = JSON.parse(localStorage.getItem('videoHistoryTags'));
    if (this.videoHistoryTags === null) {
      this.videoHistoryTags = [];
    }
  }

  updateFrameHistoryTags(newTags: string[]) {
    for (const tag of newTags) {
      if (tag.length > 0 && this.frameHistoryTags.indexOf(tag) === -1) {
        this.frameHistoryTags.unshift(tag);
        if (this.frameHistoryTags.length > 20) {
          this.frameHistoryTags.pop();
        }
      }
    }
    localStorage.setItem('frameHistoryTags', JSON.stringify(this.frameHistoryTags));
  }
  getFrameHistoryTags(): string[] {
    return this.frameHistoryTags;
  }
  loadFrameHistoryTags() {
    this.frameHistoryTags = JSON.parse(localStorage.getItem('frameHistoryTags'));
    if (this.frameHistoryTags === null) {
      this.frameHistoryTags = [];
    }
  }
}
