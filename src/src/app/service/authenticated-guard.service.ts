import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  constructor(private http: HttpService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (this.http.authInfo.isAuthenticated) {
      return new Observable<boolean>(
        (subscriber) => {
          subscriber.next(true);
          subscriber.complete();
        }
      );
    }

    /*
    if (route.queryParamMap.has('access-token') && route.queryParamMap.has('account') && route.queryParamMap.has('username')
      && (state.url.includes('/search?') || state.url.includes('/search-result?'))) {
      const accessToken = route.queryParamMap.get('access-token');
      const account = route.queryParamMap.get('account');
      const username = route.queryParamMap.get('username');
      if (this.http.isPlatformUser(accessToken, account, username)) {
        return new Observable<boolean>(
          (subscriber) => {
            subscriber.next(true);
            subscriber.complete();
          }
        );
      }

      return new Observable<boolean>(
        (subscriber) => {
          this.http.accessTokenLogin(accessToken, account, username,
            () => {
              subscriber.next(true);
              subscriber.complete();
            },
            (summary, particulars) => {
              subscriber.next(false);
              subscriber.complete();
            }
          );
        }
      );
    }
    */

    return new Observable<boolean>(
      (subscriber) => {
        this.router.navigate([ 'login' ]);
        subscriber.next(false);
        subscriber.complete();
      }
    );
  }
}
