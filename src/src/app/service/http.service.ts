import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Reply, Status, summary, particulars, toMediaTypeShow, toMediaSecondShow, toDescriptionShow } from '../common/reply';
import { MediaSearch } from '../common/media-search-reply';
import { PageReply } from '../common/page';
import * as md5 from 'js-md5';
import { UserSearch } from '../common/user-search-reply';
import { LogSearch } from '../common/log-search-reply';
import { TagSearch } from '../common/tag-search-reply';
import { LoginReply } from '../common/login-reply';
import { compress } from '../common/compress-image';

declare const $: JQueryStatic;

@Injectable()
export class HttpService {
  baseUrl = 'http://localhost:8080/media-search';
  initialPassword = '123456';
  // 用户密码加密使用盐
  // return md5.hex(this.encryptMask2 + md5.hex(this.encryptMask1 + password + this.encryptMask1) + this.encryptMask2);
  encryptMask1 = '$$encrypt-media-search-1$$';
  encryptMask2 = '$$encrypt-media-search-2$$';
  // 外部接口加密使用盐
  // md5.hex(this.extifsSalt1 + md5.hex(json) + this.extifsSalt2);
  extifsSalt1 = '$$extifs-salt1$$';
  extifsSalt2 = '$$extifs-salt2$$';

  isExtifsMode = false;

  loginPage: string;
  authInfo: {
    isAuthenticated: boolean;
    firstLogin: boolean;
    userId: number;
    userType: number;
    account: string;
    accessToken: string;
    username: string;
    secretKey: string;
  };

  searchInfo: {
    isByText: boolean;

    request: {
      pageCurrent: number;
      perPageTotalCount: number;
      mediaType: number;
      firstSearch: number;

      prevText: string;
      text: string;
      image: File;
      imageName: string;
    };

    reply: {
      page: PageReply;

      course: {
        list: MediaSearch.Course[];
        grid: MediaSearch.Course[][];
      };

      resource: {
        list: MediaSearch.Resource[];
        grid: MediaSearch.Resource[][];
      };
    };
  };

  isUserContextMenuPopup = false;
  $userContextMenuMask: JQuery<HTMLElement>;
  $userContextMenu: JQuery<HTMLElement>;

  isToolTipShow = false;
  $tooltip: JQuery<HTMLElement>;

  showAddUserDialog = false;
  addUserDialogPosition = { left: 0, top: 0 };
  addUserParams: { ok: () => void };

  showDeleteUserDialog = false;
  deleteUserDialogPosition = { left: 0, top: 0 };
  deleteUserParams: { accounts: string[], ok: () => void };

  showModifyUserDialog = false;
  modifyUserParams: { oldUser: UserSearch.User, newUser: UserSearch.User, ok: () => void };
  modifyUserDialogPosition = { left: 0, top: 0 };

  // homeViewUrl = '/home/user-management';
  homeViewUrlChange: (url: string) => void;

  constructor(private http: HttpClient, private router: Router) {
    this.loginPage = '/search';

    const authInfo = sessionStorage.getItem('authInfo');
    if (authInfo && (authInfo.length > 0)) {
      this.authInfo = JSON.parse(authInfo);
    } else {
      this.resetAuthInfo();
    }

    this.resetSearchInfo();
    this.getConfig();
  }

  toBoolean(value: string): boolean {
    return (value && (value === 'true')) ? true : false;
  }
  toInt(value: string): number {
    return (value) ? parseInt(value, 10) : 0;
  }
  toString(value: string): string {
    return (value) ? value : '';
  }

  resetAuthInfo() {
    sessionStorage.removeItem('authInfo');
    this.authInfo = {
      isAuthenticated: false,
      firstLogin: false,
      userId: null,
      userType: null,
      account: null,
      accessToken: null,
      username: null,
      secretKey: null,
    };
  }
  setAuthUsername(username: string) {
    this.authInfo.username = username;
    sessionStorage.setItem('authInfo', JSON.stringify(this.authInfo));
  }
  setSearchImage(image: File) {
    if (image) {
      this.searchInfo.request.imageName = image.name;
      this.searchInfo.request.image = image;
    } else {
      this.searchInfo.request.imageName = '';
      this.searchInfo.request.image = null;
    }
  }
  isPlatformUser(accessToken: string, account: string, username: string): boolean {
    if (this.authInfo.isAuthenticated && (this.authInfo.userType === 2) && (this.authInfo.userId === -99999)
      && (this.authInfo.account === account) && (this.authInfo.accessToken === accessToken) && (this.authInfo.username === username)) {
      return true;
    }
    return false;
  }

  resetSearchInfo() {
    this.searchInfo = {
      isByText: true,
      request: {
        pageCurrent: 1,
        perPageTotalCount: this.getConfig_perPageTotalCount(),
        mediaType: 1,
        firstSearch: 1,
        prevText: '',
        text: '',
        image: null,
        imageName: ''
      },
      reply: {
        page: null,
        course: {
          list: [],
          grid: [],
        },
        resource: {
          list: [],
          grid: [],
        }
      }
    };
  }
  clearMediaSearchReply() {
    this.searchInfo.reply.course.list = [];
    this.searchInfo.reply.course.grid = [];
    this.searchInfo.reply.resource.list = [];
    this.searchInfo.reply.resource.grid = [];
  }
  splitSearchReply(list: any[], grid: any[][], ecall?: (item: any) => void) {
    let tmp: any[];
    for (let i = 0; i < list.length; ++i) {
      if (ecall) {
        ecall(list[i]);
      }

      if ((i % 4) === 0) {
        tmp = [ list[i] ];
        grid.push(tmp);
      } else {
        tmp.push(list[i]);
      }
    }
  }

  reply(reply: Reply, ok?: (data) => void, fail?: (summary, particulars) => void) {
    if (reply.status === Status.Ok) {
      if (ok) {
        ok(reply.data);
      }
    } else {
      if ((!this.isExtifsMode) && (reply.status === Status.PermissionDenied)) {
        this.router.navigate([ '/login' ]);
        this.resetAuthInfo();
      }

      if (fail) {
        fail(summary(reply), particulars(reply));
      }
    }
  }
  postJSON(url: string, params: any, ok?: (data) => void, fail?: (summary, particulars) => void, finish?: () => void) {
    console.log(url, 'request', params);

    this.http.post(
      this.baseUrl + url,
      params,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json;charset=UTF-8'),
        observe: 'body',
        withCredentials: true,
        responseType: 'json'
      }
    ).subscribe(
      (reply: Reply) => {
        console.log(url, 'reply', reply);
        this.reply(reply, ok, fail);
      },
      (error) => {
        if (fail) {
          fail(error.statusText, error.message);
        }
      },
      () => {
        if (finish) {
          finish();
        }
      }
    );
  }
  postFORM(url: string, params: any, ok: (data) => void, fail?: (summary, particulars) => void, finish?: () => void) {
    console.log(url, 'request', params);

    const form: FormData = new FormData();
    for (const name in params) {
      if (params.hasOwnProperty(name)) {
        const value = params[name];
        if (value != null) {
          if (value.name) {
            form.append(name, value, value.name);
          } else {
            form.append(name, value);
          }
        }
      }
    }

    this.http.post(this.baseUrl + url, form,
      {
        observe: 'body',
        withCredentials: true,
        responseType: 'json'
      }
    ).subscribe(
        (reply: Reply) => {
          console.log(url, 'reply', reply);
          this.reply(reply, ok, fail);
        },
        (error) => {
          if (fail) {
            fail(error.statusText, error.message);
          }
        },
        () => {
          if (finish) {
            finish();
          }
        }
      );
  }

  /*
  postFiles(url: string, files: FileList, ok: (data) => void, fail?: (summary, particulars) => void, finish?: () => void) {
    if (files.length > 0) {
      const form: FormData = new FormData();
      for (let i = 0; i < files.length; ++i) {
        form.append(i.toString(10), files[i], files[i].name);
      }

      this.http.post(this.base + url, form,
        {
          observe: 'body',
          withCredentials: true,
          responseType: 'json'
        }
      ).subscribe(
          (reply: Reply) => {
            console.log(reply);
            if (reply.status === 'Ok') {
              if (ok) {
                ok(reply.data);
              }
            } else if (fail) {
              fail(summary(reply), particulars(reply));
            }
          },
          (error) => {
            if (fail) {
              fail(error.statusText, error.message);
            }
          },
          () => {
            if (finish) {
              finish();
            }
          }
        );
    } else {
      if (fail) {
        fail(Status.summary(Status.InvalidArgument), '未选择文件');
      }
      if (finish) {
        finish();
      }
    }
  }
  */

  getConfig() {
    const win: any = window;
    console.log('width: ', win.screen.width, 'height: ', win.screen.height);
    if (win.configs) {
      console.log('configs: ', win.configs);
      this.baseUrl = win.configs.baseUrl;
      this.searchInfo.request.perPageTotalCount = win.configs.mediaPerPageNum;
    }
  }
  getConfig_perPageTotalCount(): number {
    const win: any = window;
    if (win.configs) {
      return win.configs.mediaPerPageNum;
    }
    return 10;
  }
  getVerificationCodeUrl(): string {
    return this.baseUrl + '/verification-code' + '?index=' + new Date().getTime();
  }

  encrypt(password: string): string {
    return md5.hex(this.encryptMask2 + md5.hex(this.encryptMask1 + password + this.encryptMask1) + this.encryptMask2);
  }
  extifsEncrypt(json: string): string {
    return md5.hex(this.extifsSalt1 + md5.hex(json) + this.extifsSalt2);
  }

  login(account: string, password: string, verificationCode: string, ok: (data) => void, fail: (summary, particulars) => void) {
    this.postJSON('/authentication/login',
      {
        account: account,
        password: password,
        verificationCode: verificationCode
      },
      (data: LoginReply) => {
        this.authInfo.isAuthenticated = true;
        this.authInfo.firstLogin = data.firstLogin;
        this.authInfo.userId = data.userId;
        this.authInfo.userType = data.userType;
        this.authInfo.account = account;
        this.authInfo.accessToken = null;
        this.authInfo.username = data.username;
        sessionStorage.setItem('authInfo', JSON.stringify(this.authInfo));
        ok(data);
      },
      fail
    );
  }
  logout() {
    this.postJSON('/authentication/logout', {});
    this.router.navigate(['/login']);
    this.resetAuthInfo();
    this.resetSearchInfo();
  }

  searchByText(pageCurrent: number) {
    if (!this.isExtifsMode) {
      this.searchInfo.request.pageCurrent = pageCurrent;
      if (this.searchInfo.request.text && this.searchInfo.request.text.length > 0) {
        this.postFORM('/search/byText',
          {
            pageCurrent: this.searchInfo.request.pageCurrent,
            perPageTotalCount: this.searchInfo.request.perPageTotalCount,
            mediaType: this.searchInfo.request.mediaType,
            firstSearch: this.searchInfo.request.firstSearch,
            text: this.searchInfo.request.text
          },
          (data: MediaSearch.Reply) => {
            if (data) {
              this.searchInfo.reply.page = data.page;
              this.clearMediaSearchReply();
              if (this.searchInfo.request.mediaType === 1) { // 课程：1，资源：2，片段：3
                if (data.courses && (data.courses.length > 0)) {
                  this.searchInfo.reply.course.list = data.courses;
                  this.splitSearchReply(this.searchInfo.reply.course.list, this.searchInfo.reply.course.grid,
                    (item: MediaSearch.Course) => {
                      item.courseDescriptionShow = toDescriptionShow(item.courseDescription);
                      if (item.mediaSecond || (item.mediaSecond === 0)) {
                        item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                      }
                    }
                  );
                }
              } else {
                if (data.resources && (data.resources.length > 0)) {
                  this.searchInfo.reply.resource.list = data.resources;
                  this.splitSearchReply(this.searchInfo.reply.resource.list, this.searchInfo.reply.resource.grid,
                    (item: MediaSearch.Resource) => {
                      item.mediaDescriptionShow = toDescriptionShow(item.mediaDescription);
                      if (item.mediaSecond || (item.mediaSecond === 0)) {
                        item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                      }
                    }
                  );
                }
              }
            } else {
              this.searchInfo.reply.page = null;
              this.clearMediaSearchReply();
            }
          },
          (summary0, particulars0) => {
            // alert(particulars0);
            this.searchInfo.reply.page = null;
            this.clearMediaSearchReply();
          }
        );
      }
    } else {
      this.searchInfo.request.pageCurrent = pageCurrent;
      if (this.searchInfo.request.text && this.searchInfo.request.text.length > 0) {
        this.postFORM('/extifs/search/byText',
          {
            pageCurrent: this.searchInfo.request.pageCurrent,
            perPageTotalCount: this.searchInfo.request.perPageTotalCount,
            mediaType: this.searchInfo.request.mediaType,
            firstSearch: this.searchInfo.request.firstSearch,
            text: this.searchInfo.request.text,
            account: this.authInfo.account,
            username: this.authInfo.username,
            secretKey: this.extifsEncrypt(
              '{' +
                '"/extifs/search/byText":{' +
                  '"pageCurrent":"' + this.searchInfo.request.pageCurrent + '",' +
                  '"perPageTotalCount":"' + this.searchInfo.request.perPageTotalCount + '",' +
                  '"mediaType":"' + this.searchInfo.request.mediaType + '",' +
                  // '"firstSearch":"' + this.searchInfo.request.firstSearch + '",' +
                  '"text":"' + this.searchInfo.request.text.toUpperCase() + '",' +
                  '"user":{' +
                    '"account":"' + this.authInfo.account.toUpperCase() + '",' +
                    '"username":"' + this.authInfo.username.toUpperCase() + '"' +
                  '}' +
                '}' +
              '}'
            )
          },
          (data: MediaSearch.Reply) => {
            if (data) {
              this.searchInfo.reply.page = data.page;
              this.clearMediaSearchReply();
              if (this.searchInfo.request.mediaType === 1) { // 课程：1，资源：2，片段：3
                if (data.courses && (data.courses.length > 0)) {
                  this.searchInfo.reply.course.list = data.courses;
                  this.splitSearchReply(this.searchInfo.reply.course.list, this.searchInfo.reply.course.grid,
                    (item: MediaSearch.Course) => {
                      item.courseDescriptionShow = toDescriptionShow(item.courseDescription);
                      if (item.mediaSecond || (item.mediaSecond === 0)) {
                        item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                      }
                    }
                  );
                }
              } else {
                if (data.resources && (data.resources.length > 0)) {
                  this.searchInfo.reply.resource.list = data.resources;
                  this.splitSearchReply(this.searchInfo.reply.resource.list, this.searchInfo.reply.resource.grid,
                    (item: MediaSearch.Resource) => {
                      item.mediaDescriptionShow = toDescriptionShow(item.mediaDescription);
                      if (item.mediaSecond || (item.mediaSecond === 0)) {
                        item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                      }
                    }
                  );
                }
              }
            } else {
              this.searchInfo.reply.page = null;
              this.clearMediaSearchReply();
            }
          },
          (summary0, particulars0) => {
            // alert(particulars0);
            this.searchInfo.reply.page = null;
            this.clearMediaSearchReply();
          }
        );
      }
    }
  }
  searchByImage(pageCurrent: number) {
    this.searchInfo.request.pageCurrent = pageCurrent;
    if (this.searchInfo.request.image) {
      compress(this.searchInfo.request.image,
        (image: File) => {
          this.searchInfo.request.image = image;
          if (!this.isExtifsMode) {
            this.postFORM('/search/byImage',
              {
                pageCurrent: this.searchInfo.request.pageCurrent,
                perPageTotalCount: this.searchInfo.request.perPageTotalCount,
                mediaType: this.searchInfo.request.mediaType,
                firstSearch: this.searchInfo.request.firstSearch,
                image: this.searchInfo.request.image
              },
              (data: MediaSearch.Reply) => {
                if (data) {
                  this.searchInfo.reply.page = data.page;
                  this.clearMediaSearchReply();
                  if (this.searchInfo.request.mediaType === 1) { // 课程：1，资源：2，片段：3
                    if (data.courses && (data.courses.length > 0)) {
                      this.searchInfo.reply.course.list = data.courses;
                      this.splitSearchReply(this.searchInfo.reply.course.list, this.searchInfo.reply.course.grid,
                        (item: MediaSearch.Course) => {
                          item.courseDescriptionShow = toDescriptionShow(item.courseDescription);
                          if (item.mediaSecond || (item.mediaSecond === 0)) {
                            item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                          }
                        }
                      );
                    }
                  } else {
                    if (data.resources && (data.resources.length > 0)) {
                      this.searchInfo.reply.resource.list = data.resources;
                      this.splitSearchReply(this.searchInfo.reply.resource.list, this.searchInfo.reply.resource.grid,
                        (item: MediaSearch.Resource) => {
                          item.mediaDescriptionShow = toDescriptionShow(item.mediaDescription);
                          if (item.mediaSecond || (item.mediaSecond === 0)) {
                            item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                          }
                        }
                      );
                    }
                  }
                } else {
                  this.searchInfo.reply.page = null;
                  this.clearMediaSearchReply();
                }
              },
              (summary0, particulars0) => {
                // alert(particulars0);
                this.searchInfo.reply.page = null;
                this.clearMediaSearchReply();
              }
            );
          } else {
            this.postFORM('/extifs/search/byImage',
              {
                pageCurrent: this.searchInfo.request.pageCurrent,
                perPageTotalCount: this.searchInfo.request.perPageTotalCount,
                mediaType: this.searchInfo.request.mediaType,
                firstSearch: this.searchInfo.request.firstSearch,
                image: this.searchInfo.request.image,
                account: this.authInfo.account,
                username: this.authInfo.username,
                secretKey: this.extifsEncrypt(
                  '{' +
                    '"/extifs/search/byImage":{' +
                      '"pageCurrent":"' + this.searchInfo.request.pageCurrent + '",' +
                      '"perPageTotalCount":"' + this.searchInfo.request.perPageTotalCount + '",' +
                      '"mediaType":"' + this.searchInfo.request.mediaType + '",' +
                      // '"firstSearch":"' + this.searchInfo.request.firstSearch + '",' +
                      '"image":"' + this.searchInfo.request.imageName.toUpperCase() + '",' +
                      '"user":{' +
                        '"account":"' + this.authInfo.account.toUpperCase() + '",' +
                        '"username":"' + this.authInfo.username.toUpperCase() + '"' +
                      '}' +
                    '}' +
                  '}'
                )
              },
              (data: MediaSearch.Reply) => {
                if (data) {
                  this.searchInfo.reply.page = data.page;
                  this.clearMediaSearchReply();
                  if (this.searchInfo.request.mediaType === 1) { // 课程：1，资源：2，片段：3
                    if (data.courses && (data.courses.length > 0)) {
                      this.searchInfo.reply.course.list = data.courses;
                      this.splitSearchReply(this.searchInfo.reply.course.list, this.searchInfo.reply.course.grid,
                        (item: MediaSearch.Course) => {
                          item.courseDescriptionShow = toDescriptionShow(item.courseDescription);
                          if (item.mediaSecond || (item.mediaSecond === 0)) {
                            item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                          }
                        }
                      );
                    }
                  } else {
                    if (data.resources && (data.resources.length > 0)) {
                      this.searchInfo.reply.resource.list = data.resources;
                      this.splitSearchReply(this.searchInfo.reply.resource.list, this.searchInfo.reply.resource.grid,
                        (item: MediaSearch.Resource) => {
                          item.mediaDescriptionShow = toDescriptionShow(item.mediaDescription);
                          if (item.mediaSecond || (item.mediaSecond === 0)) {
                            item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
                          }
                        }
                      );
                    }
                  }
                } else {
                  this.searchInfo.reply.page = null;
                  this.clearMediaSearchReply();
                }
              },
              (summary0, particulars0) => {
                // alert(particulars0);
                this.searchInfo.reply.page = null;
                this.clearMediaSearchReply();
              }
            );
          }
        }
      );
    }
  }
  search(pageCurrent: number) {
    if (this.searchInfo.isByText) {
      this.searchByText(pageCurrent);
    } else {
      this.searchByImage(pageCurrent);
    }
  }

  addUserInfo(account: string, username: string, description: string, ok: (data) => void, fail: (summary, particulars) => void) {
    this.postJSON('/user-management/add-info',
      {
        account: account,
        username: username,
        description: description
      },
      ok,
      fail
    );
  }
  removeUserInfo(userId: number, username: string, ok: () => void, fail: (summary, particulars) => void) {
    this.postJSON('/user-management/remove-info',
      {
        userId: userId,
        username: username
      },
      ok,
      fail
    );
  }
  removeUserInfos(users: { userId: number, username: string }[], ok: () => void, fail: (summary, particulars) => void) {
    this.postJSON('/user-management/remove-infos',
      {
        users: users
      },
      ok,
      fail
    );
  }
  modifyUserInfo(userId: number, oldUsername: string, newUsername: string, resetPassword: boolean, description: string, ok: (data) => void, fail: (summary, particulars) => void) {
    this.postJSON('/user-management/modify-info',
      {
        userId: userId,
        oldUsername: oldUsername,
        newUsername: newUsername,
        resetPassword: resetPassword,
        description: description
      },
      ok,
      fail
    );
  }
  searchUserInfo(pageCurrent: number, perPageTotalCount: number, username: string, ok: (data: UserSearch.Reply) => void, fail: (summary, particulars) => void) {
    this.postJSON('/user-management/search-info',
      {
        pageCurrent: pageCurrent,
        perPageTotalCount: perPageTotalCount,
        username: username
      },
      ok,
      fail
    );
  }
  modifyUserPassword(userId: number, oldUsername: string, newUsername: string, oldPassword: string, newPassword: string, ok: (data) => void, fail: (summary, particulars) => void) {
    this.postJSON('/user-management/modify-password',
      {
        userId: userId,
        oldUsername: oldUsername,
        newUsername: newUsername,
        oldPassword: oldPassword,
        newPassword: newPassword
      },
      ok,
      fail
    );
  }

  searchLog(pageCurrent: number, perPageTotalCount: number, username: string, level: number, operType: number, beginTime: string, endTime: string, content: string,
    ok: (data: LogSearch.Reply) => void, fail: (summary, particulars) => void) {
    this.postJSON('/log-management/search-log',
      {
        pageCurrent: pageCurrent,
        perPageTotalCount: perPageTotalCount,
        username: username,
        level: level,
        operType: operType,
        time: {
          begin: beginTime,
          end: endTime
        },
        content: content
      },
      ok,
      fail
    );
  }

  searchVideo(pageCurrent: number, perPageTotalCount: number, mediaName: string, tags: string, beginTime: string, endTime: string, framesTagged: boolean,
    ok: (data: TagSearch.Reply) => void, fail: (summary, particulars) => void) {
    this.postFORM('/taglib/list/video',
      {
        pageCurrent: pageCurrent,
        perPageTotalCount: perPageTotalCount,
        mediaName: mediaName,
        tags: tags,
        beginTime: beginTime,
        endTime: endTime,
        framesTagged: framesTagged
      },
      ok,
      fail
    );
  }

  searchVideoFrame(mediaId: string, tags: string, beginSecond: number, endSecond: number,
    ok: (data: TagSearch.VideoFrame) => void, fail: (summary, particulars) => void) {
      // this.frameTagSearchReply = { video: {mediaId: '1', mediaUrlId: '1', mediaType: '视频',
      //                                 mediaName: '测试', framesTagged: true},
      // frames: [{imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '标签1', mediaSecond: 1, isDefaultFrame: true, tagArray: ['']},
      // {imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '标签1,标签2,标签3,标签4,标签5,标签6,标签7,标签8', mediaSecond: 20, isDefaultFrame: true, tagArray: ['']},
      // {imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '标签1,标签2,标签3,标签4,标签5,标签6,标签7,标签8,标签9', mediaSecond: 68, isDefaultFrame: true, tagArray: ['']},
      // {imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '标签0,标签1,标签2,标签3,标签4,标签5,标签6,标签7,标签8,标签9,标签10,标签11,标签12,标签13,标签14,标签15,标签16,标签17,标签18,标签19',
      //     mediaSecond: 670, isDefaultFrame: true, tagArray: ['']},
      // {imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '标签0,标签1,标签2,标签3,标签4,标签5,标签6,标签7,标签8,标签9,标签10,标签11,标签12,标签13,标签14,标签15,标签16,标签17,标签18,标签19,标签21,标签22,标签23,标签24,标签25',
      //     mediaSecond: 38610, isDefaultFrame: true, tagArray: ['']},
      // {imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '标签1', mediaSecond: 3800, isDefaultFrame: true, tagArray: ['']},
      // {imageId: '111', imageType: 'image', imageCdn: 'ttt', tags: '', mediaSecond: 3800, isDefaultFrame: true, tagArray: ['']}] };
      // {
      //   this.showFrameTags = [];
      //   let tmp: any[];
      //   for (let i = 0; i < this.frameTagSearchReply.frames.length; ++i) {
      //     if ((i % 4) === 0) {
      //       tmp = [ ];
      //       this.showFrameTags.push(tmp);
      //     }

      //     const frame = this.frameTagSearchReply.frames[i];
      //     frame.tagArray = frame.tags.length === 0 ? [] : frame.tags.split(',');
      //     tmp.push(frame);
      //   }
      // }
      // ok();
      // return;

      this.postFORM('/taglib/list/frame',
        {
          mediaId: mediaId,
          tags: tags,
          beginSecond: beginSecond,
          endSecond: endSecond
        },
        ok,
        fail
      );
  }

  updateVideoStatus(mediaId: string, framesTagged: boolean,
    ok: () => void, fail: (summary, particulars) => void) {
    // ok();
    // return;
    this.postFORM('/taglib/update/videoStatus',
      {
        mediaId: mediaId,
        framesTagged: framesTagged
      },
      ok,
      fail
    );
  }

  searchAllTags(ok: (tags: string[]) => void, fail: (summary, particulars) => void) {
    // ok(['测试', '测试测试', '测试失败', '非测试', '保留', '调试', '课程', '视频']);
    // return;
    this.http.get(
      this.baseUrl + '/taglib/list/tag',
      {
        observe: 'body',
        withCredentials: true,
        responseType: 'json'
      }
    ).subscribe(
      (reply: Reply) => {
        this.reply(reply, (data: any) => { ok(data.tags); }, fail);
      },
      (error) => {
        if (fail) {
          fail(error.statusText, error.message);
        }
      },
      () => {
      }
    );
  }

  updateVideoDefaultFrame(mediaId: string, imageId: string, ok: () => void, fail: (summary, particulars) => void) {
    // ok();
    // return;
    this.postFORM('/taglib/update/videoDefaultFrame',
      {
        mediaId: mediaId,
        imageId: imageId
      },
      ok,
      fail
    );
  }

  addImagesTags(mediaId: string, imageIds: string, tags: string, ok: () => void, fail: (summary, particulars) => void) {
    // ok();
    // return;
    this.postFORM('/taglib/add/imageTags',
      {
        mediaId: mediaId,
        imageIds: imageIds,
        tags: tags
      },
      ok,
      fail
    );
  }

  removeImagesTags(mediaId: string, imageIds: string, tags: string, ok: () => void, fail: (summary, particulars) => void) {
    // ok();
    // return;
    this.postFORM('/taglib/remove/imageTags',
      {
        mediaId: mediaId,
        imageIds: imageIds,
        tags: tags
      },
      ok,
      fail
    );
  }

  openUserContextMenu(left: number, top: number) {
    if (!this.isUserContextMenuPopup) {
      this.isUserContextMenuPopup = true;
      this.$userContextMenuMask = $('<div class="dialog-mask"></div>')
                                    .on('click', () => {
                                        this.closeUserContextMenu();
                                      }
                                    )
                                    .appendTo(window.document.body);
      this.$userContextMenu = $('<div class="dialog user-context-menu"></div>')
                                .append($('<div class="user-context-menu-item">退出</div>')
                                          .on('click', () => {
                                              this.closeUserContextMenu();
                                              this.userLogout();
                                            }
                                          )
                                        )
                                .append($('<div class="user-context-menu-item">修改密码</div>')
                                          .on('click', () => {
                                              this.closeUserContextMenu();
                                              this.userModifyPassword();
                                            }
                                          )
                                        )
                                .css({ left: left, top: top })
                                .appendTo(window.document.body);
    }
  }
  closeUserContextMenu() {
    if (this.isUserContextMenuPopup) {
      this.isUserContextMenuPopup = false;
      this.$userContextMenuMask.remove();
      this.$userContextMenuMask = undefined;
      this.$userContextMenu.remove();
      this.$userContextMenu = undefined;
    }
  }
  openToolTip(left: number, top: number, text: string) {
    if (!this.isToolTipShow) {
      this.isToolTipShow = true;
      this.$tooltip = $('<div class="tooltip">' + text + '</div>')
                        .css({ left: left, top: top })
                        .appendTo(window.document.body);
    } else {
      this.$tooltip.css({ left: left, top: top });
    }
  }
  closeToolTip() {
    if (this.isToolTipShow) {
      this.isToolTipShow = false;
      this.$tooltip.remove();
      this.$tooltip = undefined;
    }
  }

  openAddUserDialog(ok: () => void) {
    this.addUserParams = { ok: ok };
    this.addUserDialogPosition = {
      left: ($(window).width() - 420) / 2,
      top: ($(window).height() - 405) / 2
    };
    this.showAddUserDialog = true;
  }
  openDeleteUserDialog(accounts: string[], ok: () => void) {
    this.deleteUserParams = { accounts: accounts, ok: ok };
    this.deleteUserDialogPosition = {
      left: ($(window).width() - 420) / 2,
      top: ($(window).height() - 405) / 2
    };
    this.showDeleteUserDialog = true;
  }
  openModifyUserDialog(user: UserSearch.User, ok: () => void) {
    this.modifyUserParams = {
      oldUser: user,
      newUser: {
        userId: user.userId,
        account: user.account,
        username: user.username,
        description: user.description,
        createTime: user.createTime,
        checked: false,
        currentLine: false,
        resetPassword: false
      },
      ok: ok
    };
    this.modifyUserDialogPosition = {
      left: ($(window).width() - 420) / 2,
      top: ($(window).height() - 405) / 2
    };
    this.showModifyUserDialog = true;
  }

  userLogout() {
    this.logout();
  }
  userModifyPassword() {
    if (this.authInfo.userType === 1) {
      this.router.navigate([ '/modify-password-admin' ]);
    } else if (this.authInfo.userType === 3) {
      this.router.navigate([ '/modify-password' ]);
    }
  }
}

