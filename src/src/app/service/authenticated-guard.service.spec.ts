import { TestBed, inject } from '@angular/core/testing';

import { AuthenticatedGuard } from './authenticated-guard.service';

describe('AuthenticatedGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticatedGuard]
    });
  });

  it('should be created', inject([AuthenticatedGuard], (service: AuthenticatedGuard) => {
    expect(service).toBeTruthy();
  }));
});
