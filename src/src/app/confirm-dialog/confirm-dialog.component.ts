import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mms-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {
  @Input() title = '提示';
  @Input() text = '';
  @Output() confirm = new EventEmitter<boolean>();
  constructor() {
  }

  ngOnInit() {
  }
}
