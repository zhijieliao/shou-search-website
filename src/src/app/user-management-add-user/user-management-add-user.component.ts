import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../service/http.service';
import { Validator, validate_account, validate_username, validate_description } from '../common/validator';

@Component({
  selector: 'mms-user-management-add-user',
  templateUrl: './user-management-add-user.component.html',
  styleUrls: ['./user-management-add-user.component.css']
})
export class UserManagementAddUserComponent implements OnInit {
  account = '';
  username = '';
  description = '';

  @Input() params: { ok: () => void };

  validators: Validator[];
  canSave: boolean;

  constructor(public http: HttpService) {
    this.validators = [
      new Validator(
        (): { success: boolean, message: string } => {
          this.account = this.account.trim();
          return validate_account(this.account);
        }
      ),
      new Validator(
        (): { success: boolean, message: string } => {
          this.username = this.username.trim();
          return validate_username(this.username);
        }
      ),
      new Validator(
        (): { success: boolean, message: string } => {
          this.description = this.description.trim();
          return validate_description(this.description);
        }
      )
    ];
  }

  ngOnInit() {
  }

  accountValidate(): Validator {
    return this.validators[0];
  }

  usernameValidate(): Validator {
    return this.validators[1];
  }

  descriptionValidate(): Validator {
    return this.validators[2];
  }

  validate() {
    this.canSave = true;
    for (let i = 0; i < this.validators.length; ++i) {
      const result = this.validators[i].validate(true);
      if (!result.success) {
        this.canSave = false;
        alert(result.message);
        break;
      }
    }
  }

  closeDialog() {
    this.http.showAddUserDialog = false;
  }

  ok() {
    this.validate();
    if (this.canSave) {
      this.closeDialog();

      this.http.addUserInfo(this.account, this.username, this.description,
        () => {
          if (this.params && this.params.ok) {
            this.params.ok();
          }

          alert('添加用户成功');
        },
        (summary, particulars) => {
          alert(particulars);
        }
      );
    }
  }
  cancel() {
    this.closeDialog();
  }
}
