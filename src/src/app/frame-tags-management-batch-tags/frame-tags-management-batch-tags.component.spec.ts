import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameTagsManagementBatchTagsComponent } from './frame-tags-management-batch-tags.component';

describe('FrameTagsManagementBatchTagsComponent', () => {
  let component: FrameTagsManagementBatchTagsComponent;
  let fixture: ComponentFixture<FrameTagsManagementBatchTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameTagsManagementBatchTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameTagsManagementBatchTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
