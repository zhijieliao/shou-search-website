import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { HttpService } from '../service/http.service';
import { SearchHistoryService } from '../service/search-history.service';
import { TagSearch } from '../common/tag-search-reply';

@Component({
  selector: 'mms-frame-tags-management-batch-tags',
  templateUrl: './frame-tags-management-batch-tags.component.html',
  styleUrls: ['./frame-tags-management-batch-tags.component.css']
})
export class FrameTagsManagementBatchTagsComponent implements OnInit {
  @Input() mediaId: string;
  @Input() selectedFrames: TagSearch.Frame[];
  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  @Output() update: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('inputTags')
  inputTags: ElementRef;

  imageIds = '';

  historyTags: string[];
  selectedTags = '';
  selectedTagsArray: string[] = [];
  searchMatchTags: string[] = [];

  remindTags: string[] = [];

  isConfirmDlgShow = false;
  isRemoveTag = false;
  confirmDlgTitle = '';
  confirmDlgText = '';

  isInformationDlgShow = false;
  informationDlgText = '';
  constructor(private http: HttpService, private searchHistory: SearchHistoryService) {
    this.historyTags = this.searchHistory.getFrameHistoryTags();

    this.http.searchAllTags(
      (tags: string[]) => { this.remindTags = tags; },
      () => {}
    );
  }

  ngOnInit() {
    for (let i = 0; i < this.selectedFrames.length; ++i) {
      this.imageIds = i === 0 ? this.imageIds.concat(this.selectedFrames[i].imageId) :
        this.imageIds.concat(',' + this.selectedFrames[i].imageId);
    }
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(event) {
    this.searchMatchTags = [];
  }

  updateHistoryTags() {
    this.searchHistory.updateFrameHistoryTags(this.selectedTagsArray);
    this.historyTags = this.searchHistory.getFrameHistoryTags();
  }

  resetSelected() {
    this.selectedTags = '';
    this.selectedTagsArray = [];
    this.searchMatchTags = [];
  }

  inputChanged(text: string, cursorPos: number) {
    const validText = text.replace(/，/g, ',');
    this.selectedTagsArray = validText.length > 0 ? validText.split(',') : [];
    this.searchMatchTags = [];

    const startPos = validText.substr(0, cursorPos).lastIndexOf(',');
    const currentTag = startPos === -1 ? validText.substring(0, cursorPos) :
            validText.substring(startPos + 1, cursorPos);

    if (currentTag.length > 0) {
      for ( const tag of this.remindTags ) {
        if (tag.indexOf(currentTag) >= 0) {
          this.searchMatchTags.push(tag);
        }
      }
    }
  }

  isSelected(tag: string) {
    return this.selectedTagsArray.indexOf(tag) !== -1;
  }

  changeSelected(tag: string) {
    const pos = this.selectedTagsArray.indexOf(tag);
    if ( pos === -1) {
      this.selectedTagsArray.push(tag);
      this.selectedTags = this.selectedTags.length === 0 ? this.selectedTags.concat(tag) : this.selectedTags.concat(',' + tag);
    } else {
      this.selectedTagsArray.splice(pos, 1);
      this.selectedTags = this.contactArr(this.selectedTagsArray);
    }
  }

  contactArr(tagArr: string[]): string {
    let newTags = '';
    for (let i = 0; i < tagArr.length; ++i) {
      newTags = i === 0 ? newTags.concat(tagArr[i]) : newTags.concat(',' + tagArr[i]);
    }
    return newTags;
  }

  allSelection() {
    for (const tag of this.historyTags) {
      const pos = this.selectedTagsArray.indexOf(tag);
      if ( pos === -1) {
        this.selectedTagsArray.push(tag);
        this.selectedTags = this.selectedTags.length === 0 ? this.selectedTags.concat(tag) : this.selectedTags.concat(',' + tag);
      }
    }
  }

  invertSelection() {
    for (const tag of this.historyTags) {
      this.changeSelected(tag);
    }
  }

  verifyTags(): boolean {
    const validTags: string[] = [];
    const win: any = window;
    const reg = win.configs ? new RegExp(win.configs.tagValidRegExp) : new RegExp('');
    const maxLength = win.configs ? win.configs.tagMaxLength : 0;
    for (const tag of this.selectedTagsArray) {
      if (tag.length > 0) {
        if (!reg.test(tag)) {
          this.openInformationDlg('标签[' + tag + ']含有非法字符，请重新输入!');
          return false;
        } else if (maxLength && maxLength > 0 && tag.length > maxLength) {
          this.openInformationDlg('标签[' + tag + ']超过最大长度[' + maxLength + ']，请重新输入!');
          return false;
        } else {
          validTags.push(tag);
        }
      }
    }

    if (validTags.length === 0) {
      this.openInformationDlg('标签内容为空，请选择或输入标签!');
      return false;
    }
    return true;
  }

  updateInputTag(newTag: string) {
    const cursorPos = this.inputTags.nativeElement.selectionEnd;
    const startPos = this.selectedTags.substr(0, cursorPos).replace(/，/g, ',').lastIndexOf(',');
    const leftStr = startPos === -1 ? newTag : this.selectedTags.substr(0, startPos + 1) + newTag;
    this.selectedTags = leftStr + this.selectedTags.substr(cursorPos, this.selectedTags.length - 1);
    this.selectedTagsArray = this.selectedTags.length > 0 ? this.selectedTags.replace(/，/g, ',').split(',') : [];
    this.searchMatchTags = [];

    this.inputTags.nativeElement.value = this.selectedTags;
    this.inputTags.nativeElement.focus();
    this.inputTags.nativeElement.setSelectionRange(leftStr.length, leftStr.length);
  }

  addFrameTags() {
    if (!this.verifyTags()) {
      return;
    }
    this.isConfirmDlgShow = true;
    this.isRemoveTag = false;
    this.confirmDlgTitle = '添加标签';
    this.confirmDlgText = '确定添加标签?';
  }

  // removeFrameOneTag(tag: string) {
  //   if (confirm('确定删除标签?')) {
  //     this.http.removeImagesTags(this.mediaId, this.imageIds, tag,
  //       () => {
  //         this.update.emit();
  //       },
  //       () => {}
  //     );
  //   }
  // }

  removeFrameTags() {
    if (!this.verifyTags()) {
      return;
    }
    this.isConfirmDlgShow = true;
    this.isRemoveTag = true;
    this.confirmDlgTitle = '删除标签';
    this.confirmDlgText = '确定删除标签?';
  }

  confirmDlgClose(confirm: boolean) {
    this.isConfirmDlgShow = false;

    if (confirm) {
      if (this.isRemoveTag) {
        const validTags: string[] = [];
        for (const tag of this.selectedTagsArray) {
          if (tag.length > 0 && validTags.indexOf(tag) === -1) {
            validTags.push(tag);
          }
        }

        this.http.removeImagesTags(this.mediaId, this.imageIds, this.contactArr(validTags),
          () => {
            setTimeout(() => { this.update.emit(); }, 1000);
            this.updateHistoryTags();
            this.resetSelected();
            this.openInformationDlg('删除标签成功!');
          },
          (summary: string, particulars: string) => {
            this.openInformationDlg('删除标签失败,失败原因:' + particulars);
          }
        );
      } else {
        const validTags: string[] = [];
        for (const tag of this.selectedTagsArray) {
          if (tag.length > 0 && validTags.indexOf(tag) === -1) {
            validTags.push(tag);
          }
        }

        this.http.addImagesTags(this.mediaId, this.imageIds, this.contactArr(validTags),
          () => {
            setTimeout(() => { this.update.emit(); }, 1000);
            this.updateHistoryTags();
            this.resetSelected();

            this.openInformationDlg('添加标签成功!');
          },
          (summary: string, particulars: string) => {
            this.openInformationDlg('添加标签失败,失败原因:' + particulars);
          }
        );
      }
    }
  }

  openInformationDlg(text: string) {
    this.isInformationDlgShow = true;
    this.informationDlgText = text;
  }

  closeInformationDlg() {
    this.isInformationDlgShow = false;
    this.informationDlgText = '';
  }
}
