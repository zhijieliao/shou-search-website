import { Component, OnInit, OnChanges, SimpleChanges, Input, Output,
  EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { COMPOSITION_BUFFER_MODE } from '@angular/forms';
import { isNumber } from 'util';

@Component({
  selector: 'mms-input-time',
  templateUrl: './input-time.component.html',
  styleUrls: ['./input-time.component.css']
})
export class InputTimeComponent implements OnInit, OnChanges {
  @Input() time = '';
  @Output() update = new EventEmitter<string>();
  @ViewChild('hourInput')
  hourInput: ElementRef;
  @ViewChild('minuteInput')
  minuteInput: ElementRef;
  @ViewChild('secondInput')
  secondInput: ElementRef;

  timeArr: string[] = ['', '', ''];
  focusInput = -1;
  constructor(private thisElement: ElementRef) { }

  @HostListener('window:click', ['$event'])
  onWindowClick(event) {
    if (this.hourInput.nativeElement.contains(event.target)) {
      this.focusInput = 0;
    } else if (this.minuteInput.nativeElement.contains(event.target)) {
      this.focusInput = 1;
    } else if (this.secondInput.nativeElement.contains(event.target)) {
      this.focusInput = 2;
    } else if (!this.thisElement.nativeElement.contains(event.target)) {
      this.focusInput = -1;
    }
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const tmpTime = this.time.split(':');
    if (tmpTime.length === 3) {
      this.timeArr = tmpTime;
    } else {
      this.timeArr = ['', '', ''];
    }
  }

  getFocusElement(pos: number): ElementRef {
    switch (pos) {
      case 0:
      {
        return this.hourInput;
      }
      case 1:
      {
        return this.minuteInput;
      }
      case 2:
      {
        return this.secondInput;
      }
      default:
      return undefined;
    }
  }

  upClick() {
    if (this.focusInput < 0) {
      this.secondInput.nativeElement.focus();
      this.focusInput = 2;
    }
    if (this.focusInput >= 0) {
      const value = this.timeArr[this.focusInput].length > 0 ?
            parseInt(this.timeArr[this.focusInput], 10) : 0;
      this.timeArr[this.focusInput] = value >= 59 ? value.toString() : (value + 1).toString();
      if (this.timeArr[this.focusInput].length === 1) {
        this.timeArr[this.focusInput] = '0' + this.timeArr[this.focusInput];
      }
    }

    this.sendResult();
  }

  downClick() {
    if (this.focusInput < 0) {
      this.secondInput.nativeElement.focus();
      this.focusInput = 2;
    }
    const value = this.timeArr[this.focusInput].length > 0 ?
            parseInt(this.timeArr[this.focusInput], 10) : 0;
    this.timeArr[this.focusInput] = value <= 0 ? value.toString() : (value - 1).toString();
    if (this.timeArr[this.focusInput].length === 1) {
      this.timeArr[this.focusInput] = '0' + this.timeArr[this.focusInput];
    }
    this.sendResult();
  }

  resetClick() {
    this.timeArr = ['', '', ''];
    this.sendResult();
  }

  parseInt(s: string, radix?: number): number {
    const number = parseInt(s, 10);
    return isNaN(number) ? 0 : number;
  }

  keyDownEvent(event: any, focusNumber: number): boolean {
    if (event.key === 'Backspace') {
      return true;
    } else if (isNaN(parseInt(event.key, 10))) {
      return false;
    }
    const focusInputElement = this.getFocusElement(focusNumber);
    const inputPosStart = focusInputElement.nativeElement.selectionStart;
    const inputPosEnd = focusInputElement.nativeElement.selectionEnd;

    if (inputPosEnd >= (inputPosStart + 2)) {
      return true;
    }
    if ( inputPosEnd === inputPosStart && this.timeArr[focusNumber].length >= 2) {
      return false;
    }
    if (inputPosStart === 0) {
      if (parseInt(event.key, 10) > 5 && this.timeArr[focusNumber].length > 0 &&
          inputPosEnd < this.timeArr[focusNumber].length) {
        return false;
      }
      return true;
    } else if (inputPosStart === 1) {
      if (parseInt(this.timeArr[focusNumber].substring(0, 1), 10) > 5) {
        return false;
      }
      return true;
    }
    return false;
  }

  keyUpEvent(focusNumber: number) {
    this.timeArr[focusNumber] = this.timeArr[focusNumber].replace(/[\u4e00-\u9fa5]/g, '');
  }

  inputChange(focusNumber: number) {
    const focusInputElement = this.getFocusElement(focusNumber);
    const inputPosEnd = focusInputElement.nativeElement.selectionEnd;
    if (this.timeArr[focusNumber].length === 2 && parseInt(this.timeArr[focusNumber].charAt(0), 10) > 5) {
      if (inputPosEnd === 1) {
        this.timeArr[focusNumber] = this.timeArr[focusNumber].slice(1, 2);
      } else {
        this.timeArr[focusNumber] = this.timeArr[focusNumber].slice(0, 1);
      }
      focusInputElement.nativeElement.value = this.timeArr[focusNumber];
      return;
    }

    this.sendResult();
  }

  sendResult() {
    const isEmpty = this.timeArr[0].length === 0 && this.timeArr[1].length === 0 && this.timeArr[2].length === 0;
    if (isEmpty) {
      this.update.emit('');
    } else {
      this.update.emit(this.timeArr[0] + ':' + this.timeArr[1] + ':' + this.timeArr[2]);
    }
  }
}
