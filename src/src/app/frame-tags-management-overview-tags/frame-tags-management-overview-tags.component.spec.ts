import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameTagsManagementOverviewTagsComponent } from './frame-tags-management-overview-tags.component';

describe('FrameTagsManagementOverviewTagsComponent', () => {
  let component: FrameTagsManagementOverviewTagsComponent;
  let fixture: ComponentFixture<FrameTagsManagementOverviewTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameTagsManagementOverviewTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameTagsManagementOverviewTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
