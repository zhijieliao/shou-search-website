import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { HttpService } from '../service/http.service';
import { TagSearch } from '../common/tag-search-reply';

@Component({
  selector: 'mms-frame-tags-management-overview-tags',
  templateUrl: './frame-tags-management-overview-tags.component.html',
  styleUrls: ['./frame-tags-management-overview-tags.component.css']
})
export class FrameTagsManagementOverviewTagsComponent implements OnInit {
  @Input() videoFrame: TagSearch.VideoFrame;
  @Input() frame: TagSearch.Frame;
  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  @Output() update: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('inputTags')
  inputTags: ElementRef;

  selectedTags = '';
  selectedTagsArray: string[] = [];
  searchMatchTags: string[] = [];

  remindTags: string[] = [];

  isConfirmDlgShow = false;
  isRemoveTag = false;
  removeTag = '';
  confirmDlgTitle = '';
  confirmDlgText = '';

  isInformationDlgShow = false;
  informationDlgText = '';

  constructor(public http: HttpService) {
    this.http.searchAllTags(
      (tags: string[]) => { this.remindTags = tags; },
      () => {}
    );
  }

  ngOnInit() {
  }

  gotoVideoDetail() {
    // window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + this.videoFrame.video.mediaUrlId +
    //               '&second=' + this.frame.mediaSecond);
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(event) {
    this.searchMatchTags = [];
  }

  resetSelected() {
    this.selectedTags = '';
    this.selectedTagsArray = [];
    this.searchMatchTags = [];
  }

  secondToTime(totalSecond: number): string {
    const hour = Math.floor(totalSecond / 3600);
    const minute = Math.floor((totalSecond % 3600) / 60);
    const second = (totalSecond % 3600) % 60;

    const hourStr = hour > 9 ? hour : '0' + hour;
    const minuteStr = minute > 9 ? minute : '0' + minute;
    const secondStr = second > 9 ? second : '0' + second;
    return hourStr + ':' + minuteStr + ':' + secondStr;
  }

  inputChanged(text: string, cursorPos: number) {
    const validText = text.replace(/，/g, ',');
    this.selectedTagsArray = validText.length > 0 ? validText.split(',') : [];
    this.searchMatchTags = [];

    const startPos = validText.substr(0, cursorPos).lastIndexOf(',');
    const currentTag = startPos === -1 ? validText.substring(0, cursorPos) :
                validText.substring(startPos + 1, cursorPos);

    if (currentTag.length > 0) {
      for ( const tag of this.remindTags ) {
        if (tag.indexOf(currentTag) >= 0) {
          this.searchMatchTags.push(tag);
        }
      }
    }
  }

  isSelected(tag: string) {
    return this.selectedTagsArray.indexOf(tag) !== -1;
  }

  changeSelected(tag: string) {
    const pos = this.selectedTagsArray.indexOf(tag);
    if ( pos === -1) {
      this.selectedTagsArray.push(tag);
      this.selectedTags = this.selectedTags.length === 0 ? this.selectedTags.concat(tag) : this.selectedTags.concat(',' + tag);
    } else {
      this.selectedTagsArray.splice(pos, 1);
      this.selectedTags = this.contactArr(this.selectedTagsArray);
    }
  }

  contactArr(tagArr: string[]): string {
    let newTags = '';
    for (let i = 0; i < tagArr.length; ++i) {
      newTags = i === 0 ? newTags.concat(tagArr[i]) : newTags.concat(',' + tagArr[i]);
    }
    return newTags;
  }

  allSelection() {
    for (const tag of this.frame.tagArray) {
      const pos = this.selectedTagsArray.indexOf(tag);
      if ( pos === -1) {
        this.selectedTagsArray.push(tag);
        this.selectedTags = this.selectedTags.length === 0 ? this.selectedTags.concat(tag) : this.selectedTags.concat(',' + tag);
      }
    }
  }

  invertSelection() {
    for (const tag of this.frame.tagArray) {
      this.changeSelected(tag);
    }
  }

  updateInputTag(newTag: string) {
    const cursorPos = this.inputTags.nativeElement.selectionEnd;
    const startPos = this.selectedTags.substr(0, cursorPos).replace(/，/g, ',').lastIndexOf(',');
    const leftStr = startPos === -1 ? newTag : this.selectedTags.substr(0, startPos + 1) + newTag;
    this.selectedTags = leftStr  + this.selectedTags.substr(cursorPos, this.selectedTags.length - 1);
    this.selectedTagsArray = this.selectedTags.length > 0 ? this.selectedTags.replace(/，/g, ',').split(',') : [];
    this.searchMatchTags = [];

    this.inputTags.nativeElement.value = this.selectedTags;
    this.inputTags.nativeElement.focus();
    this.inputTags.nativeElement.setSelectionRange(leftStr.length, leftStr.length);
  }

  verifyTags(): boolean {
    const validTags: string[] = [];
    const win: any = window;
    const reg = win.configs ? new RegExp(win.configs.tagValidRegExp) : new RegExp('');
    const maxLength = win.configs ? win.configs.tagMaxLength : 0;
    for (const tag of this.selectedTagsArray) {
      if (tag.length > 0) {
        if (!reg.test(tag)) {
          this.openInformationDlg('标签[' + tag + ']含有非法字符，请重新输入!');
          return false;
        } else if (maxLength && maxLength > 0 && tag.length > maxLength) {
          this.openInformationDlg('标签[' + tag + ']超过最大长度[' + maxLength + ']，请重新输入!');
          return false;
        } else {
          validTags.push(tag);
        }
      }
    }

    if (validTags.length === 0) {
      this.openInformationDlg('标签内容为空，请选择或输入标签!');
      return false;
    }
    return true;
  }

  addFrameTags() {
    if (!this.verifyTags()) {
      return;
    }

    this.isConfirmDlgShow = true;
    this.isRemoveTag = false;
    this.confirmDlgTitle = '添加标签';
    this.confirmDlgText = '确定添加标签?';
  }

  removeFrameOneTag(tag: string) {
    this.isConfirmDlgShow = true;
    this.isRemoveTag = true;
    this.confirmDlgTitle = '删除标签';
    this.confirmDlgText = '确定删除标签?';
    this.removeTag = tag;
  }

  removeFrameTags() {
    if (!this.verifyTags()) {
      return;
    }
    this.isConfirmDlgShow = true;
    this.isRemoveTag = true;
    this.confirmDlgTitle = '删除标签';
    this.confirmDlgText = '确定删除标签?';
  }

  confirmDlgClose(confirm: boolean) {
    if (confirm) {
      if (this.isRemoveTag) {
        if (this.removeTag.length > 0) {
          this.http.removeImagesTags(this.videoFrame.video.mediaId, this.frame.imageId, this.removeTag,
              () => {
                setTimeout( () => { this.update.emit(); }, 1000);
                this.openInformationDlg('删除标签成功!');
              },
              (summary: string, particulars: string) => {
                this.openInformationDlg('删除标签失败,失败原因:' + particulars);
              }
            );
        } else {
          const validTags: string[] = [];
          for (const tag of this.selectedTagsArray) {
            if (tag.length > 0 && validTags.indexOf(tag) === -1) {
              validTags.push(tag);
            }
          }

          this.http.removeImagesTags(this.videoFrame.video.mediaId, this.frame.imageId, this.contactArr(validTags),
            () => {
              setTimeout( () => { this.update.emit(); }, 1000);
              this.openInformationDlg('删除标签成功!');
              this.resetSelected();
            },
            (summary: string, particulars: string) => {
              this.openInformationDlg('删除标签失败,失败原因:' + particulars);
            }
          );
        }
      } else {
        const validTags: string[] = [];
        for (const tag of this.selectedTagsArray) {
          if (tag.length > 0 && validTags.indexOf(tag) === -1) {
            validTags.push(tag);
          }
        }

        this.http.addImagesTags(this.videoFrame.video.mediaId, this.frame.imageId, this.contactArr(validTags),
          () => {
            setTimeout(() => { this.update.emit(); }, 1000);
            this.openInformationDlg('添加标签成功!');
            this.resetSelected();
            // this.frame.tagArray.concat(validTags);
          },
          (summary: string, particulars: string) => {
            this.openInformationDlg('添加标签失败,失败原因:' + particulars);
          }
        );
      }
    }

    this.isConfirmDlgShow = false;
    this.isRemoveTag = false;
    this.confirmDlgTitle = '';
    this.confirmDlgText = '?';
    this.removeTag = '';
  }

  openInformationDlg(text: string) {
    this.isInformationDlgShow = true;
    this.informationDlgText = text;
  }

  closeInformationDlg() {
    this.isInformationDlgShow = false;
    this.informationDlgText = '';
  }
}
