import { Component, AfterViewInit, Input } from '@angular/core';
import { HttpService } from '../service/http.service';
import { PageReply } from '../common/page';
import { UserSearch } from '../common/user-search-reply';
import { Router } from '@angular/router';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements AfterViewInit {
  private _allChecked: boolean;
  @Input()
  set allChecked(allChecked: boolean) {
    this._allChecked = allChecked;
    for (const user of this.users) {
      user.checked = allChecked;
    }
  }
  get allChecked(): boolean {
    return this._allChecked;
  }

  username: string;

  currentPage: number;
  perPageTotalCount: number;
  page: PageReply;
  users: UserSearch.User[];

  dialogShow: boolean;
  $dialogMask: JQuery<HTMLElement>;
  $dialog: JQuery<HTMLElement>;

  constructor(public http: HttpService, public router: Router) {
    const win: any = window;
    if (win.configs) {
      this.perPageTotalCount = win.configs.userInfoPerPageNum;
    } else {
      this.perPageTotalCount = 10;
    }
    this.username = '';
    this.currentPage = 1;
    this.page = { begin: 1, current: 1, end: 0, totalCount: 0 };
    this.users = [];
    /*
    if (!this.http.authInfo.isAuthenticated) {
      this.users = [
        { userId: 0, account: 'test1test1test1test1test1test1test1test1test1', username: '测试使用1', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 1, account: 'test2', username: '测试使用2', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 2, account: 'test3', username: '测试使用3', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 3, account: 'test4', username: '测试使用4测试使用4测试使用4测试使用4', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 4, account: 'test5', username: '测试使用5', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 5, account: 'test6', username: '测试使用6', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 6, account: 'test7', username: '测试使用7', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 7, account: 'test8', username: '测试使用8', description: '测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false },
        { userId: 8, account: 'test9', username: '测试使用9', description: '测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用', createTime: '2018-04-01 09:00:00', checked: false, currentLine: false, resetPassword: false }
      ];
    }
    */

    this.dialogShow = false;
    this.$dialogMask = null;
    this.$dialog = null;
  }

  ngAfterViewInit() {
    if (this.http.homeViewUrlChange) {
      this.http.homeViewUrlChange(this.router.url);
    }
    if (this.http.authInfo.isAuthenticated) {
      this.searchUser();
    }
  }

  clearCurrentLine() {
    for (const user of this.users) {
      user.currentLine = false;
    }
  }
  currentLineChanged(user: UserSearch.User) {
    this.clearCurrentLine();
    user.checked = true;
    user.currentLine = true;
  }

  searchUser() {
    this.currentPageChanged(1);
  }

  closeDialog() {
    if (this.dialogShow) {
      this.dialogShow = false;
      this.$dialogMask.remove();
      this.$dialogMask = null;
      this.$dialog.remove();
      this.$dialog = null;
    }
  }

  addUser() {
    this.http.openAddUserDialog(
      () => {
        this.searchUser();
      }
    );
  }

  modifyUser() {
    for (const user of this.users) {
      if (user.currentLine) {
        this.operModifyUser(user);
        break;
      }
    }
  }

  removeUsers() {
    const users: { userId: number, username: string }[] = [];
    for (const user of this.users) {
      if (user.checked) {
        users.push({ userId: user.userId, username: user.username });
      }
    }

    if (users.length > 0) {
      const accounts: string[] = [];
      for (const user of this.users) {
        if (user.checked) {
          accounts.push(user.account);
        }
      }

      this.http.openDeleteUserDialog(
        accounts,
        () => {
          this.http.removeUserInfos(users,
            () => {
              this.searchUser();
              alert('删除用户成功');
            },
            (summary, particulars) => {
              alert(particulars);
            }
          );
        }
      );
    }
  }

  operRemoveUser(user: UserSearch.User) {
    this.http.openDeleteUserDialog(
      [user.account],
      () => {
        this.http.removeUserInfo(user.userId, user.username,
          () => {
            this.searchUser();
            alert('删除用户成功');
          },
          (summary, particulars) => {
            alert(particulars);
          }
        );
      }
    );
  }

  operModifyUser(user: UserSearch.User) {
    this.http.openModifyUserDialog(user,
      () => {
        this.searchUser();
      }
    );
  }

  currentPageChanged(currentPage: number) {
    this.http.searchUserInfo(currentPage, this.perPageTotalCount, this.username,
      (data) => {
        if (data) {
          if (data.page && data.page.end > 0) {
            if (data.users && (data.users.length > 0)) {
              this.page = data.page;
              this.users = data.users;
              for (const user of this.users) {
                user.checked = false;
                user.currentLine = false;
              }
            } else {
              this.page = null;
              this.users = [];
            }
          } else {
            this.page = null;
            this.users = [];
          }
        } else {
          this.page = null;
          this.users = [];
        }
      },
      () => {
        this.page = null;
        this.users = [];
      }
    );
  }
}
