import { PageReply } from './page';

export namespace TagSearch {

    export interface Reply {
        page: PageReply;
        videos: Video[];
    }

    export interface Video {
        mediaId: string;
        mediaUrlId: string;
        mediaType: string;
        mediaName: string;
        mediaDefaultFrameCdn: string;
        mediaCreateTime: string;
        tags: string;
        framesTagged: boolean;
    }

    export interface VideoFrame {
        video: {
            mediaId: string;
            mediaUrlId: string;
            mediaType: string;
            mediaName: string;
            framesTagged: boolean;
        };

        frames: Frame[];
    }

    export interface Frame {
        imageId:	string;
        imageType:	string;
        imageCdn:	string;
        tags:	string;
        mediaSecond:	number;
        isDefaultFrame:	boolean;

        tagArray: string[];
    }
}
