export function mediaIsVideo(mediaType: string): boolean {
    const win: any = window;

    let  mediaTypeOfVideo = [ '视频' ];
    if (win.configs && win.configs.mediaTypeOfVideo) {
        mediaTypeOfVideo = win.configs.mediaTypeOfVideo;
    }

    for (let i = 0; i < mediaTypeOfVideo.length; ++i) {
        if (mediaTypeOfVideo[i] === mediaType) {
            return true;
        }
    }
    return false;
}
