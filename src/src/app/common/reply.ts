export class Reply {
    status: string;
    message: string;
    data: any;
}

export class Status {
    static Ok = 'Ok';
    static Failed = 'Failed';
    static Error = 'Error';
    static InvalidArgument = 'InvalidArgument';
    static PermissionDenied = 'PermissionDenied';
    static ExceptionOccurred = 'ExceptionOccurred';
    static Other = 'Other';

    static summary(status: string): string {
        if (status === Status.Ok) {
            return '成功';
        } else if (status === Status.Failed) {
            return '失败';
        } else if (status === Status.Error) {
            return '错误';
        } else if (status === Status.InvalidArgument) {
            return '无效参数';
        } else if (status === Status.PermissionDenied) {
            return '未授权';
        } else if (status === Status.ExceptionOccurred) {
            return '发生异常';
        } else if (status === Status.Other) {
            return '其它';
        } else {
            return '未知';
        }
    }
}

export function summary(reply: Reply): string {
    return Status.summary(reply.status);
}

export function particulars(reply: Reply): string {
    return reply.message;
}

export function toMediaTypeShow(mediaType: string): string {
    switch (mediaType) {
        case 'video':
            return '视频';
        case 'audio':
            return '音频';
        case 'image':
            return '图片';
        default:
            return '文档';
    }
}

export function toMediaSecondShow(mediaSecondShow: number): string {
    const t1 = mediaSecondShow % 3600;
    const t2 = [NString(Math.floor(mediaSecondShow / 3600)), NString(Math.floor(t1 / 60)), NString(t1 % 60)];
    return t2.join(':');
}

export function toDescriptionShow(description: string): string {
    if (description && (description.length > 120)) {
        description = description.substr(0, 120) + '...';
    }
    return description;
}

function NString(n: number): string {
    return (n >= 10) ? String(n) : ('0' + n);
}
