export class Validator {
    doValidate: () => { success: boolean, message: string };
    result: (success: boolean, message: string, focus: boolean) => void;

    constructor(doValidate: () => { success: boolean, message: string }) {
        this.doValidate = doValidate;
    }

    validate(focus: boolean): { success: boolean, message: string } {
        if (this.doValidate) {
            const result = this.doValidate();
            if (this.result) {
                this.result(result.success, result.message, focus);
            }
            return result;
        }
        return {
            success: false,
            message: '未知错误'
        };
    }
}

function check_invalid_chars(str: string) {
    const invalid_chars = '`~!#$%^&*()+={}[]|\\:;"\'<,>?/ ';
    for (let i = 0; i < invalid_chars.length; ++i) {
        if (str.indexOf(invalid_chars[i]) >= 0) {
            return true;
        }
    }
    return false;
}

export function validate_account(account: string): { success: boolean, message: string } {
    if (account.length === 0) {
        return {
            success: false,
            message: '账号不能为空'
        };
    } else if (check_invalid_chars(account)) {
        return {
            success: false,
            message: '账号不能包含`~!#$%^&*()+={}[]|\\:;"\'<,>?/和空格字符'
        };
    } else if (account.length < 1) {
        return {
            success: false,
            message: '账号的长度太短，账号长度不能低于1个字'
        };
    } else if (account.length > 128) {
        return {
            success: false,
            message: '账号的长度太长，账号长度不能超过128个字'
        };
    }
    return {
        success: true,
        message: ''
    };
}

export function validate_username(username: string): { success: boolean, message: string } {
    if (username.length === 0) {
        return {
            success: false,
            message: '用户名不能为空'
        };
    } else if (username.length > 16) {
        return {
            success: false,
            message: '用户名的长度太长，用户名长度1~16个字符'
        };
    }
    return {
        success: true,
        message: ''
    };
}

export function validate_password(password: string, text: string): { success: boolean, message: string } {
    if (password.length === 0) {
        return {
            success: false,
            message: text + '密码不能为空'
        };
    } else if (password.length < 8) {
        return {
            success: false,
            message: text + '密码太短，密码长度8~32个字符'
        };
    } else if (password.length > 32) {
        return {
            success: false,
            message: text + '密码太长，密码长度8~32个字符'
        };
    }
    return {
        success: true,
        message: ''
    };
}

export function validate_description(description: string): { success: boolean, message: string } {
    if (description.length > 200) {
        return {
            success: false,
            message: '描述的长度太长，描述长度不能超过200个字符'
        };
    }
    return {
        success: true,
        message: ''
    };
}
