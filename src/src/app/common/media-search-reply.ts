import { PageReply } from './page';

export namespace MediaSearch {

    export interface Reply {
        page: PageReply;
        courses: Course[];
        resources: Resource[];
    }

    export interface Course {
        courseId: string;
        courseUrlId: string;    // shou系统课程在url上的ID
        courseType: string;     // 课程分类: 视频, 图书, 网络课程
        courseName: string;     // shou系统课程名称
        courseCoverCdn: string; // 课程封面图片在CDN上的URL地址
        courseDescription: string;
        mediaUrlId: string;         // 媒体源文件在shou系统详情页的URL的ID
        mediaType: string;      // shou系统媒体源类型，如："视频"、"图书"、"网络课程"、"文档"
        mediaSecond: number;   // 视频源中的位置，单位：秒, 只有mediaType为"视频"时才存在

        mediaSecondShow: string;
        courseDescriptionShow: string;
    }

    export interface Resource {
        imageId: string;            // 图片ID
        imageType: string;          // 媒体源分类, 二选一: "image"：图片; "video"：视频
        imageCdn: string;           // 图片在CDN上的URL地址
        mediaId: string;            // 媒体源ID
        mediaUrlId: string;         // 媒体源文件在shou系统详情页的URL的ID
        mediaType: string;          // shou系统媒体源类型，如："视频"、"图书"、"网络课程"、"文档"
        mediaName: string;          // 媒体源名称
        mediaCdn: string;           // 媒体源文件在CDN上的URL地址
        mediaCoverCdn: string;      // 媒体源文件封面图在CDN上的URL地址
        mediaSecond: number;   // 视频源中的位置，单位：秒, 只有mediaType为"视频"时才存在
        mediaDescription: string;

        mediaSecondShow: string;
        mediaDescriptionShow: string;
    }
}

