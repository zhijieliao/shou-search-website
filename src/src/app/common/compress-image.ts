export function compress(file: File, callback: (file: File) => void) {
    const _2M = 2 * 1024 * 1024;

    if (file.size < _2M) {
        callback(file);
    } else {
        console.log('图片超过2M，正在进行压缩。');
        const fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = (e1: any) => {

            const img = new Image();
            img.src = e1.target.result;
            img.onload = (e2) => {
                const width = Math.floor(img.width * 0.8);
                const height = Math.floor(img.height * 0.8);

                const canvas = document.createElement('canvas');
                const ctx = canvas.getContext('2d');

                const attrw = document.createAttribute('width');
                attrw.nodeValue = String(width);
                canvas.setAttributeNode(attrw);

                const attrh = document.createAttribute('height');
                attrh.nodeValue = String(height);
                canvas.setAttributeNode(attrh);

                ctx.drawImage(img, 0, 0, width, height);
                canvas.toBlob(
                    (result) => {
                        const blobs = [result];
                        const compressFile = new File(blobs, file.name, { type: file.type });
                        if (compressFile.size < _2M) {
                            callback(compressFile);
                        } else {
                            compress(compressFile, callback);
                        }
                    },
                    file.type
                );
            };
        };
    }
}
