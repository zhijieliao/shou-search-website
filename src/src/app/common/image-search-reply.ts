import { PageReply } from './page';

export namespace ImageSearch {

    export interface Reply {
        page: PageReply;
        courses: Course[];
        resources: Resource[];
    }

    export interface Course {
        courseType: string;     // 课程分类
                                // 视频"
                                // 图书"
                                // 网络课程"
        courseUrlId: string;    // shou系统课程在url上的ID
        courseName: string;     // shou系统课程名称
        courseCoverCdn: string; // 课程封面图片在CDN上的URL地址
        courseDescription: string;
        mediaUrlId: string;     // 媒体源文件在shou系统详情页的URL上的ID
        mediaSecond: number;    // 视频截帧时的秒数，单位：秒
                                // 只有mediaType为audio/video时才有效"

        courseDescriptionShow: string;
    }

    export class Resource {
        imageType: string;      // 图片类型
                                // "face""：人脸图片
                                // "image""：无人脸图片
        imageWidth: number;     // 图片像素宽度
        imageHeight: number;    // 图片像素高度
        imageCdn: string;       // 图片源文件在CDN上的URL地址
        mediaType: string;      // 媒体源分类
                                // "image""：图片
                                // "video""：视频
        mediaName: string;      // 媒体源名称
        mediaUrlId: string;     // 媒体源文件在shou系统详情页的URL上的ID
        mediaCdn: string;       // 媒体源文件在CDN上的URL地址
        mediaCoverCdn: string;  // 媒体源文件封面图片在CDN上的URL地址
        mediaSecond: number;    // 视频截帧时的秒数，单位：秒
                                // 只有mediaType为audio/video时才有效"
        mediaDescription: string;

        mediaTypeShow: string;
        mediaSecondShow: string;
        mediaDescriptionShow: string;
    }
}
