export interface PageArgument {
    pageCurrent: number;
    perPageTotalCount: number;
}

export interface PageReply {
    begin: number;
    current: number;
    end: number;
    totalCount: number;
}
