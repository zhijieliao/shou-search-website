export interface LoginReply {
    firstLogin: boolean;
    userId: number;
    userType: number;
    username: string;
}
