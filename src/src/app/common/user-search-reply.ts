import { PageReply } from './page';

export namespace UserSearch {

    export interface Reply {
        page: PageReply;
        users: User[];
    }

    export interface User {
        userId: number;
        account: string;
        username: string;
        description: string;
        createTime: string;

        checked: boolean;
        currentLine: boolean;
        resetPassword: boolean;
    }
}
