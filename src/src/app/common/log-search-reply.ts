import { PageReply } from './page';

export namespace LogSearch {

    export interface Reply {
        page: PageReply;
        logs: Log[];
    }

    export interface Log {
        level: number;
        operType: number;
        username: string;
        time: string;
        content: string;
        ipaddr: string;
    }
}
