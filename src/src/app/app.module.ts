import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SearchComponent } from './search/search.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpService } from './service/http.service';
import { AuthenticatedGuard } from './service/authenticated-guard.service';
import { SearchHistoryService } from './service/search-history.service';
import { LogoComponent } from './logo/logo.component';
import { ResultComponent } from './result/result.component';
import { ModifyPasswordComponent } from './modify-password/modify-password.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { LogManagementComponent } from './log-management/log-management.component';
import { HomeComponent } from './home/home.component';
import { ResultListItemComponent } from './result-list-item/result-list-item.component';
import { ResultGridItemComponent } from './result-grid-item/result-grid-item.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PaginationButtonComponent } from './pagination-button/pagination-button.component';
import { LogLevelFormatPipe } from './pipe/log-level-format.pipe';
import { LogOperTypeFormatPipe } from './pipe/log-oper-type-format.pipe';
import { ModifyPasswordAdminComponent } from './modify-password-admin/modify-password-admin.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ExtifsSearchComponent } from './extifs-search/extifs-search.component';
import { ValidatorDirective } from './directive/validator.directive';
import { UserManagementAddUserComponent } from './user-management-add-user/user-management-add-user.component';
import { UserManagementModifyUserComponent } from './user-management-modify-user/user-management-modify-user.component';
import { ExtifsSearchResultComponent } from './extifs-search-result/extifs-search-result.component';
import { UserManagementDeleteUserComponent } from './user-management-delete-user/user-management-delete-user.component';
import { VideoTagsManagementComponent } from './video-tags-management/video-tags-management.component';
import { VideoTagsManagementMoreTagsComponent } from './video-tags-management-more-tags/video-tags-management-more-tags.component';
import { FrameTagsManagementComponent } from './frame-tags-management/frame-tags-management.component';
import { SelectTagsComponent } from './select-tags/select-tags.component';
import { FrameTagsManagementOverviewTagsComponent } from './frame-tags-management-overview-tags/frame-tags-management-overview-tags.component';
import { FrameTagsManagementBatchTagsComponent } from './frame-tags-management-batch-tags/frame-tags-management-batch-tags.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { InformationDialogComponent } from './information-dialog/information-dialog.component';
import { InputTimeComponent } from './input-time/input-time.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SearchComponent,
    LogoComponent,
    ResultComponent,
    ModifyPasswordComponent,
    UserManagementComponent,
    LogManagementComponent,
    HomeComponent,
    ResultListItemComponent,
    ResultGridItemComponent,
    PaginationComponent,
    PaginationButtonComponent,
    LogLevelFormatPipe,
    LogOperTypeFormatPipe,
    ModifyPasswordAdminComponent,
    ExtifsSearchComponent,
    ValidatorDirective,
    UserManagementAddUserComponent,
    UserManagementModifyUserComponent,
    ExtifsSearchResultComponent,
    UserManagementDeleteUserComponent,
    VideoTagsManagementComponent,
    VideoTagsManagementMoreTagsComponent,
    FrameTagsManagementComponent,
    SelectTagsComponent,
    FrameTagsManagementOverviewTagsComponent,
    FrameTagsManagementBatchTagsComponent,
    ConfirmDialogComponent,
    InformationDialogComponent,
    InputTimeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    HttpClient,
    HttpService,
    AuthenticatedGuard,
    SearchHistoryService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
