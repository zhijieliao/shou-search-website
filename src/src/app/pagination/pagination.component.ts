import { Component, EventEmitter, Input, ViewEncapsulation, ViewChild, ElementRef, Renderer2, Output } from '@angular/core';
import { PaginationButtonComponent } from './../pagination-button/pagination-button.component';
import { PageReply } from '../common/page';
import { HttpService } from '../service/http.service';

@Component({
  selector: 'mms-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PaginationComponent {
  private _page: PageReply;
  private _offsetPage: number;
  private _currentPage: number;
  private _beginButton: number;
  private _endButton: number;
  private _currentButton: number;

  buttonClass1 = 'pagination-button-select';
  buttonClass2 = 'pagination-button-hide';
  buttonClass3 = 'pagination-button-hide';
  buttonClass4 = 'pagination-button-hide';
  buttonClass5 = 'pagination-button-hide';
  buttonClass6 = 'pagination-button-hide';
  buttonClass7 = 'pagination-button-hide';

  buttonText1 = '1';
  buttonText2 = '2';
  buttonText3 = '3';
  buttonText4 = '4';
  buttonText5 = '5';
  buttonText6 = '6';
  buttonText7 = '7';

  buttonF = true;
  buttonL = true;

  @Input()
  set page(page: PageReply) {
    this._page = page;
    if (page) {
      this._currentPage = 1;
      this._offsetPage = 1;
      this._beginButton = 1;
      this._currentButton = 1;
      this.buttonF = true;
      this.buttonL = true;

      if (page && (page.end > 0)) {
          this.setButtonClass(1, 'pagination-button-select');
          this.setButtonText(1, '1');

          for (let i = 2; (i <= page.end) && (i <= 7); ++i) {
            this.setButtonClass(i, '');
            this.setButtonText(i, i.toString());
          }

          if (page.end <= 7) {
            for (let i = page.end + 1; i <= 7; ++i) {
              this.setButtonClass(i, 'pagination-button-hide');
            }
            this._endButton = this.page.end;
          } else {
            this.setButtonText(7, '...');
            this.buttonL = false;
          this._endButton = 6;
        }
      } else {
        this._endButton = 1;
      }

      while (this._currentPage < page.current) {
        this.moveNext(false);
      }
    } else {
      this._currentPage = 1;
      this._offsetPage = 1;
      this._beginButton = 1;
      this._currentButton = 1;
      this._endButton = 1;
    }
  }
  get page(): PageReply {
    return this._page;
  }
  @Output()
  currentPageChanged = new EventEmitter<number>();

  constructor(private http: HttpService) {
    this._page = undefined;
  }

  setButtonClass(i: number, buttonClass: string) {
    switch (i) {
      case 1: this.buttonClass1 = buttonClass; break;
      case 2: this.buttonClass2 = buttonClass; break;
      case 3: this.buttonClass3 = buttonClass; break;
      case 4: this.buttonClass4 = buttonClass; break;
      case 5: this.buttonClass5 = buttonClass; break;
      case 6: this.buttonClass6 = buttonClass; break;
      case 7: this.buttonClass7 = buttonClass; break;
    }
  }

  setButtonText(i: number, buttonText: string) {
    switch (i) {
      case 1: this.buttonText1 = buttonText; break;
      case 2: this.buttonText2 = buttonText; break;
      case 3: this.buttonText3 = buttonText; break;
      case 4: this.buttonText4 = buttonText; break;
      case 5: this.buttonText5 = buttonText; break;
      case 6: this.buttonText6 = buttonText; break;
      case 7: this.buttonText7 = buttonText; break;
    }
  }

  setCurrentButton(i: number) {
    this.setButtonClass(this._currentButton, '');
    this.setButtonClass(i, 'pagination-button-select');
    this._currentButton = i;
  }

  button2Page(i: number) {
    if (this._beginButton === 1) {
      return this._offsetPage + i - 1;
    } else {
      return this._offsetPage + i - 2;
    }
  }

  page2Button(page: number) {
    if (this._beginButton === 1) {
      return page - this._offsetPage + 1;
    } else {
      return page - this._offsetPage + 2;
    }
  }

  move() {
    this.buttonF = true;
    this.buttonL = true;
    if (this._beginButton === 1) {
      if (this._endButton === 6) {
        for (let i = 1; i <= 6; ++i) {
          this.setButtonClass(i, '');
          this.setButtonText(i, String(i));
        }
        this.setButtonClass(7, '');
        this.setButtonText(7, '...');
        this.buttonL = false;
      } else {
        for (let i = 1; i <= 7; ++i) {
          this.setButtonClass(i, '');
          this.setButtonText(i, String(i));
        }
      }
    } else {
      this.setButtonClass(1, '');
      this.setButtonText(1, '...');
      this.buttonF = false;
      if (this._endButton === 6) {
        for (let i = 2, j = this._offsetPage; i <= 6; ++i, ++j) {
          this.setButtonClass(i, '');
          this.setButtonText(i, String(j));
        }
        this.setButtonClass(7, '');
        this.setButtonText(7, '...');
        this.buttonL = false;
      } else {
        for (let i = 2, j = this._offsetPage; i <= 7; ++i, ++j) {
          this.setButtonClass(i, '');
          this.setButtonText(i, String(j));
        }
      }
    }
  }
  movePrev(emitEvent: boolean) {
    if (this._page) {
      if (this._currentPage > this.page.begin) {
        const button = this.page2Button(this._currentPage - 1);
        this.moveButton(button, emitEvent);
      }
    }
  }
  moveButton(buttonIndex: number, emitEvent: boolean) {
    if (this._page) {
      this._currentPage = this.button2Page(buttonIndex);
      if (buttonIndex === 1) {
        if (this._beginButton === 1) {
          if (this._currentButton !== 1) {
            this.setCurrentButton(1);
            if (emitEvent) {
              this.currentPageChanged.emit(this._currentPage);
            }
          }
        }
      } else if ((buttonIndex === 2) || (buttonIndex === 3)) {
        if (this._page.end > 7) {
          if (this._offsetPage > 1) {
            if (this._offsetPage > 3) {
              if ((this._offsetPage + 4) < this._page.end) {
                this._offsetPage -= 1;
                this._beginButton = 2;
                this._endButton = 6;
                this.move();
              } else {
                this._offsetPage -= 1;
                this._beginButton = 2;
                this._endButton = 7;
                this.move();
              }
            } else {
              this._offsetPage = 1;
              this._beginButton = 1;
              this._endButton = 6;
              this.move();
            }
          }
        }

        const button = this.page2Button(this._currentPage);
        this.setCurrentButton(button);
        if (emitEvent) {
          this.currentPageChanged.emit(this._currentPage);
        }
      } else if (buttonIndex === 4) {
        const button = this.page2Button(this._currentPage);
        this.setCurrentButton(button);
        if (emitEvent) {
          this.currentPageChanged.emit(this._currentPage);
        }
      } else if ((buttonIndex === 5) || (buttonIndex === 6)) {
        if (this._page.end > 7) {
          if (this._offsetPage > 1) {
            if ((this._offsetPage + 5) < this._page.end) {
              this._offsetPage += 1;
              this._beginButton = 2;
              this._endButton = 6;
              this.move();
            } else {
              this._beginButton = 2;
              this._endButton = 7;
              this.move();
            }
          } else {
            if ((this._offsetPage + 7) < this._page.end) {
              this._offsetPage = 3;
              this._beginButton = 2;
              this._endButton = 6;
              this.move();
            } else {
              this._offsetPage = 3;
              this._beginButton = 2;
              this._endButton = 7;
              this.move();
            }
          }
        }

        const button = this.page2Button(this._currentPage);
        this.setCurrentButton(button);
        if (emitEvent) {
          this.currentPageChanged.emit(this._currentPage);
        }
      } else if (buttonIndex === 7) {
        if (this._endButton === 7) {
          if (this._currentButton !== 7) {
            this.setCurrentButton(7);
            if (emitEvent) {
              this.currentPageChanged.emit(this._currentPage);
            }
          }
        }
      }
    }
  }
  moveNext(emitEvent: boolean) {
    if (this._page) {
      if (this._currentPage < this._page.end) {
        const button = this.page2Button(this._currentPage + 1);
        this.moveButton(button, emitEvent);
      }
    }
  }

  buttonPrev() {
    this.movePrev(true);
  }
  buttonNum(buttonIndex: number) {
    if (buttonIndex === 1) {
      if (this.buttonF) {
        this.moveButton(1, true);
      }
    } else if (buttonIndex === 7) {
      if (this.buttonL) {
        this.moveButton(7, true);
      }
    } else {
      this.moveButton(buttonIndex, true);
    }
  }
  buttonNext() {
    this.moveNext(true);
  }
}
