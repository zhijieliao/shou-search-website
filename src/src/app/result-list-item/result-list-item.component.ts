import { Component, OnInit, Input } from '@angular/core';
import { MediaSearch } from '../common/media-search-reply';
import { HttpService } from '../service/http.service';
import { mediaIsVideo } from '../common/media-is-video';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-result-list-item',
  templateUrl: './result-list-item.component.html',
  styleUrls: ['./result-list-item.component.css']
})
export class ResultListItemComponent implements OnInit {
  @Input() courseRes: MediaSearch.Course;
  @Input() resourceRes: MediaSearch.Resource;

  constructor(public http: HttpService) { }

  ngOnInit() {
  }

  isVideo(mediaType: string): boolean {
    return mediaIsVideo(mediaType);
  }
  hasSecond(second: number): boolean {
    if (second || (second === 0)) {
      return true;
    }
    return false;
  }

  gotoCourseDetail(course: MediaSearch.Course) {
    if (course.mediaUrlId) {
      if (this.isVideo(course.mediaType) && this.hasSecond(course.mediaSecond)) {
        window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?courseId=' + course.courseUrlId + '&resId=' + course.mediaUrlId + '&second=' + course.mediaSecond);
      } else {
        window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?courseId=' + course.courseUrlId + '&resId=' + course.mediaUrlId);
      }
    } else {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?courseId=' + course.courseUrlId);
    }
  }

  gotoResourceDetail(resource: MediaSearch.Resource) {
    if (this.isVideo(resource.mediaType) && this.hasSecond(resource.mediaSecond)) {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + resource.mediaUrlId + '&second=' + resource.mediaSecond);
    } else {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + resource.mediaUrlId);
    }
  }

  onLoadImg(img: HTMLImageElement) {
    /*
    const $img = $(img);
    const percent = img.naturalHeight / img.naturalWidth;
    if (percent  === 0.75) {
      $img.css('width', 140).css('height', 105);
    } else if (percent > 0.8928571428571429) {
      $img.css('width', 125 * img.naturalWidth / img.naturalHeight).css('height', 125);
    } else {
      $img.css('width', 140).css('height', 140 * img.naturalHeight / img.naturalWidth);
    }
    */
  }

  onLoadImg2(img: HTMLImageElement) {
    /*
    const $img = $(img);
    const percent = img.naturalHeight / img.naturalWidth;
    if (percent  === 0.75) {
      $img.css('width', 140).css('height', 105);
    } else if (percent > 0.75) {
      $img.css('width', 105 * img.naturalWidth / img.naturalHeight).css('height', 105);
    } else {
      $img.css('width', 140).css('height', 140 * img.naturalHeight / img.naturalWidth);
    }
    */
  }
}
