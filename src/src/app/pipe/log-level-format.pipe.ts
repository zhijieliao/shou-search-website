import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'logLevelFormat'
})
export class LogLevelFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let level: number;
    if (typeof value === 'number') {
      level = value;
    } else if (typeof value === 'string') {
      level = parseInt(value, 10);
    } else {
      return '未知';
    }

    switch (level) {
      case 1: return '严重错误';
      case 2: return '一般错误';
      case 3: return '警告';
      case 4: return '消息';
    }
    return '未知';
  }
}
