import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'logOperTypeFormat'
})
export class LogOperTypeFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let operType: number;
    if (typeof value === 'number') {
      operType = value;
    } else if (typeof value === 'string') {
      operType = parseInt(value, 10);
    } else {
      return '未知';
    }

    switch (operType) {
      case 1: return '增加';
      case 2: return '删除';
      case 3: return '修改';
      case 4: return '查询';
      case 5: return '上传';
      case 6: return '下载';
      case 7: return '登录';
      case 8: return '登出';
      case 9: return '其它';
    }
    return '未知';
  }
}
