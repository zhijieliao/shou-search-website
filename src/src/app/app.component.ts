import { Component } from '@angular/core';
import { HttpService } from './service/http.service';

@Component({
  selector: 'mms-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public http: HttpService) {

  }
}
