import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, HostListener } from '@angular/core';
import { HttpService } from '../service/http.service';
import { SearchHistoryService } from '../service/search-history.service';

@Component({
  selector: 'mms-select-tags',
  templateUrl: './select-tags.component.html',
  styleUrls: ['./select-tags.component.css']
})
export class SelectTagsComponent implements OnInit {
  @Input() selectedTags = '';
  @Input() historyTags: string[] = [];
  @Output() confirm: EventEmitter<string> = new EventEmitter<string>();
  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('inputTags')
  inputTags: ElementRef;

  selectedTagsArray: string[] = [];
  searchMatchTags: string[] = [];

  remindTags: string[] = [];

  isInformationDlgShow = false;
  informationDlgText = '';
  constructor(private http: HttpService) {
    this.http.searchAllTags(
      (tags: string[]) => { this.remindTags = tags; },
      () => {}
    );
  }

  ngOnInit() {
    this.selectedTagsArray = this.selectedTags.length > 0 ? this.selectedTags.split(',') : [];
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(event) {
    this.searchMatchTags = [];
  }

  inputChanged(text: string, cursorPos: number) {
    const validText = text.replace(/，/g, ',');
    this.selectedTagsArray = validText.length > 0 ? validText.split(',') : [];
    this.searchMatchTags = [];

    const startPos = validText.substr(0, cursorPos).lastIndexOf(',');
    const currentTag = startPos === -1 ? validText.substring(0, cursorPos) :
                        validText.substring(startPos + 1, cursorPos);

    if (currentTag.length > 0) {
      for ( const tag of this.remindTags ) {
        if (tag.indexOf(currentTag) >= 0) {
          this.searchMatchTags.push(tag);
        }
      }
    }
  }

  isSelected(tag: string) {
    return this.selectedTagsArray.indexOf(tag) !== -1;
  }

  changeSelected(tag: string) {
    const pos = this.selectedTagsArray.indexOf(tag);
    if ( pos === -1) {
      this.selectedTagsArray.push(tag);
      this.selectedTags = this.selectedTags.length === 0 ? this.selectedTags.concat(tag) : this.selectedTags.concat(',' + tag);
    } else {
      this.selectedTagsArray.splice(pos, 1);
      this.selectedTags = this.contactArr(this.selectedTagsArray);
    }
  }

  contactArr(tagArr: string[]): string {
    let newTags = '';
    for (let i = 0; i < tagArr.length; ++i) {
      newTags = i === 0 ? newTags.concat(tagArr[i]) : newTags.concat(',' + tagArr[i]);
    }
    return newTags;
  }

  verifyTags(): boolean {
    const win: any = window;
    const reg = win.configs ? new RegExp(win.configs.tagValidRegExp) : new RegExp('');
    const maxLength = win.configs ? win.configs.tagMaxLength : 0;
    for (const tag of this.selectedTagsArray) {
      if (tag.length > 0) {
        if (!reg.test(tag)) {
          this.openInformationDlg('标签[' + tag + ']含有非法字符，请重新输入!');
          return false;
        } else if (maxLength && maxLength > 0 && tag.length > maxLength) {
          this.openInformationDlg('标签[' + tag + ']超过最大长度[' + maxLength + ']，请重新输入!');
          return false;
        }
      }
    }
    return true;
  }

  confirmClick() {
    if (!this.verifyTags()) {
      return;
    }

    const norepeatArr: string[] = [];
    for (const tag of this.selectedTagsArray) {
      if ( tag.length > 0) {
        if (norepeatArr.indexOf(tag) === -1) {
          norepeatArr.push(tag);
        }
      }
    }
    this.confirm.emit(this.contactArr(norepeatArr));
  }

  allSelection() {
    for (const tag of this.historyTags) {
      const pos = this.selectedTagsArray.indexOf(tag);
      if ( pos === -1) {
        this.selectedTagsArray.push(tag);
        this.selectedTags = this.selectedTags.length === 0 ? this.selectedTags.concat(tag) : this.selectedTags.concat(',' + tag);
      }
    }
  }

  invertSelection() {
    for (const tag of this.historyTags) {
      this.changeSelected(tag);
    }
  }

  updateInputTag(newTag: string) {
    const cursorPos = this.inputTags.nativeElement.selectionEnd;
    const startPos = this.selectedTags.substr(0, cursorPos).replace(/，/g, ',').lastIndexOf(',');
    const leftStr = startPos === -1 ? newTag : this.selectedTags.substr(0, startPos + 1) + newTag;
    this.selectedTags = leftStr + this.selectedTags.substr(cursorPos, this.selectedTags.length - 1);
    this.selectedTagsArray = this.selectedTags.length > 0 ? this.selectedTags.replace(/，/g, ',').split(',') : [];
    this.searchMatchTags = [];

    this.inputTags.nativeElement.value = this.selectedTags;
    this.inputTags.nativeElement.focus();
    this.inputTags.nativeElement.setSelectionRange(leftStr.length, leftStr.length);
  }

  openInformationDlg(text: string) {
    this.isInformationDlgShow = true;
    this.informationDlgText = text;
  }

  closeInformationDlg() {
    this.isInformationDlgShow = false;
    this.informationDlgText = '';
  }
}
