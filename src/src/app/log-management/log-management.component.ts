import { HttpService } from './../service/http.service';
import { Component, AfterViewInit, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { LogSearch } from '../common/log-search-reply';
import { PageReply } from '../common/page';
import { Router } from '@angular/router';

declare const $: any;

@Component({
  selector: 'mms-log-management',
  templateUrl: './log-management.component.html',
  styleUrls: ['./log-management.component.css']
})
export class LogManagementComponent implements AfterViewInit, OnChanges {
  $window: {
    width: number;
    height: number
  };

  beginTime: string;
  endTime: string;
  level: number;
  operType: number;
  username: string;
  content: string;

  currentPage: number;
  perPageTotalCount: number;
  page: PageReply;
  logs: LogSearch.Log[];

  constructor(public http: HttpService, public router: Router) {
    this.$window = { width: window.innerWidth, height: window.innerHeight };
    window.addEventListener('resize',
      () => {
        this.$window.width = window.innerWidth;
        this.$window.height = window.innerHeight;
      }
    );
    const win: any = window;
    if (win.configs) {
      this.perPageTotalCount = win.configs.logInfoPerPageNum;
    } else {
      this.perPageTotalCount = 10;
    }

    this.level = 0;
    this.operType = 0;
    this.username = '';
    this.content = '';

    this.currentPage = 1;
    this.page = { begin: 1, current: 1, end: 0, totalCount: 0 };
    this.logs = [];

    if (!this.http.authInfo.isAuthenticated) {
      this.logs = [
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用', ipaddr: '192.168.1.1' },
        { level: 1, operType: 1, username: '系统管理员', time: '2018-04-01 09:00:00', content: '测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用测试使用', ipaddr: '192.168.1.1' }
      ];
    }
  }

  ngAfterViewInit() {
    if (this.http.homeViewUrlChange) {
      this.http.homeViewUrlChange(this.router.url);
    }
    this.dcalendarpicker();
    if (this.http.authInfo.isAuthenticated) {
      this.searchLog();
    }
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  dcalendarpicker() {
    const beginTimeInput = $('#beginTimeInput');
    beginTimeInput.dcalendarpicker();
    const endTimeInput = $('#endTimeInput');
    endTimeInput.dcalendarpicker();
  }

  NString(n: number): string {
    return (n >= 10) ? String(n) : ('0' + n);
  }

  date2String(date: Date): string {
    const d = [String(date.getFullYear()), this.NString(date.getMonth() + 1), this.NString(date.getDate())];
    return d.join('-');
  }

  datetime2String(date: Date): string {
    const d = [String(date.getFullYear()), this.NString(date.getMonth() + 1), this.NString(date.getDate())];
    const t = [this.NString(date.getHours()), this.NString(date.getMinutes()), this.NString(date.getSeconds())];
    return d.join('-') + ' ' + t.join(':');
  }

  searchLog() {
    this.currentPageChanged(1);
  }

  currentPageChanged(currentPage: number) {
    this.beginTime = $('#beginTimeInput').val();
    this.endTime = $('#endTimeInput').val();
    if (this.beginTime < this.endTime) {
      this.http.searchLog(currentPage, this.perPageTotalCount, this.username, this.level, this.operType, this.beginTime, this.endTime, this.content,
        (data) => {
          if (data) {
            if (data.page && data.page.end > 0) {
              this.page = data.page;
              this.logs = data.logs;
            } else {
              this.page = null;
              this.logs = [];
            }
          } else {
            this.page = null;
            this.logs = [];
          }
        },
        () => {
          this.page = null;
          this.logs = [];
        }
      );
    } else {
      alert('开始时间不大于结束时间！');
    }
  }
}
