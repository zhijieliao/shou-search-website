import { Router } from '@angular/router';
import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HttpService } from '../service/http.service';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {
  @ViewChild('menuItem1')
  menuItem1: ElementRef;
  @ViewChild('menuItem1Icon1')
  menuItem1Icon1: ElementRef;
  @ViewChild('menuItem1Icon2')
  menuItem1Icon2: ElementRef;
  @ViewChild('menuItem2')
  menuItem2: ElementRef;
  @ViewChild('menuItem2Icon1')
  menuItem2Icon1: ElementRef;
  @ViewChild('menuItem2Icon2')
  menuItem2Icon2: ElementRef;
  @ViewChild('menuItem3')
  menuItem3: ElementRef;
  @ViewChild('menuItem3Icon1')
  menuItem3Icon1: ElementRef;
  @ViewChild('menuItem3Icon2')
  menuItem3Icon2: ElementRef;

  @ViewChild('userDiv')
  userDiv: ElementRef;

  currentIndex: number;

  contentSize: {
    width: number;
    height: number;
  };

  constructor(public http: HttpService, public router: Router) {
    this.currentIndex = 0;

    this.contentSize = { width: (window.innerWidth - 200), height: (window.innerHeight - 110) };
    window.addEventListener('resize',
      () => {
        this.contentSize.width = window.innerWidth - 200;
        this.contentSize.height = window.innerHeight - 110;
      }
    );
    this.http.homeViewUrlChange = (url: string) => {
      switch (url) {
      case '/home/user-management':
      default:
        this.setUserManageCurrent();
        break;
      case '/home/log-management':
        this.setLogManageCurrent();
        break;
      case '/home/video-tags-management':
      case '/home/frame-tags-management':
        this.setTagManageCurrent();
        break;
      }
    };
  }

  ngAfterViewInit() {
    this.http.homeViewUrlChange(this.router.url);
  }

  setUserManageCurrent() {
    this.currentIndex = 0;
    $(this.menuItem1.nativeElement).addClass('menu-item-current');
    $(this.menuItem1Icon1.nativeElement).prop('src', 'assets/images/users_g.png');
    $(this.menuItem1Icon2.nativeElement).removeClass('display-none');
    $(this.menuItem2.nativeElement).removeClass('menu-item-current');
    $(this.menuItem2Icon1.nativeElement).prop('src', 'assets/images/journal_w.png');
    $(this.menuItem2Icon2.nativeElement).addClass('display-none');
    $(this.menuItem3.nativeElement).removeClass('menu-item-current');
    $(this.menuItem3Icon1.nativeElement).prop('src', 'assets/images/video_w.png');
    $(this.menuItem3Icon2.nativeElement).addClass('display-none');
  }

  setLogManageCurrent() {
    this.currentIndex = 1;
    $(this.menuItem1.nativeElement).removeClass('menu-item-current');
    $(this.menuItem1Icon1.nativeElement).prop('src', 'assets/images/users_w.png');
    $(this.menuItem1Icon2.nativeElement).addClass('display-none');
    $(this.menuItem2.nativeElement).addClass('menu-item-current');
    $(this.menuItem2Icon1.nativeElement).prop('src', 'assets/images/journal_g.png');
    $(this.menuItem2Icon2.nativeElement).removeClass('display-none');
    $(this.menuItem3.nativeElement).removeClass('menu-item-current');
    $(this.menuItem3Icon1.nativeElement).prop('src', 'assets/images/video_w.png');
    $(this.menuItem3Icon2.nativeElement).addClass('display-none');
  }
  setTagManageCurrent() {
    this.currentIndex = 2;
    $(this.menuItem1.nativeElement).removeClass('menu-item-current');
    $(this.menuItem1Icon1.nativeElement).prop('src', 'assets/images/users_w.png');
    $(this.menuItem1Icon2.nativeElement).addClass('display-none');
    $(this.menuItem2.nativeElement).removeClass('menu-item-current');
    $(this.menuItem2Icon1.nativeElement).prop('src', 'assets/images/journal_w.png');
    $(this.menuItem2Icon2.nativeElement).addClass('display-none');
    $(this.menuItem3.nativeElement).addClass('menu-item-current');
    $(this.menuItem3Icon1.nativeElement).prop('src', 'assets/images/video_g.png');
    $(this.menuItem3Icon2.nativeElement).removeClass('display-none');
  }
  userManage() {
    // this.setUserManageCurrent();
    this.router.navigate(['/home/user-management']);
  }
  logManage() {
    // this.setLogManageCurrent();
    this.router.navigate(['/home/log-management']);
  }
  tagManage() {
    // this.setTagManageCurrent();
    this.router.navigate(['/home/video-tags-management']);
  }

  popupUserContextMenu() {
    const $userDiv = $(this.userDiv.nativeElement);
    const position = $userDiv.position();
    this.http.openUserContextMenu(position.left, position.top + $userDiv.outerHeight());
  }
}

class MenuItemIcon {
  constructor(public icon: string[]) { }
}
