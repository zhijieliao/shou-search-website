import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtifsSearchComponent } from './extifs-search.component';

describe('ExtifsSearchComponent', () => {
  let component: ExtifsSearchComponent;
  let fixture: ComponentFixture<ExtifsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtifsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtifsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
