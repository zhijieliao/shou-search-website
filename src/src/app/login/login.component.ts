import { Router } from '@angular/router';
import { Component, AfterViewInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { HttpService } from '../service/http.service';
import { LoginReply } from '../common/login-reply';
import { validate_account, validate_password } from '../common/validator';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit, OnDestroy {
  @ViewChild('form')
  form: ElementRef;
  @ViewChild('verificationCodeImg')
  verificationCodeImg: ElementRef;
  account: string;
  password: string;
  verificationCode: string;
  isLogining: boolean;
  keypressEvent;

  constructor(public http: HttpService, public router: Router) {
    this.keypressEvent = (event: KeyboardEvent) => {
      if (event.keyCode === 13) {
        this.login();
      }
    };

    this.account = '';
    this.password = '';
    this.verificationCode = '';
    this.isLogining = false;
  }

  ngAfterViewInit() {
    window.addEventListener('keypress', this.keypressEvent);
    this.verificationCodeClicked();
    this.isLogining = false;
  }

  ngOnDestroy() {
    window.removeEventListener('keypress', this.keypressEvent);
  }

  verificationCodeClicked() {
    $(this.verificationCodeImg.nativeElement).prop('src', this.http.getVerificationCodeUrl());
  }

  login() {
    if (!this.isLogining) {
      this.isLogining = true;
      const v1 = validate_account(this.account);
      if (!v1.success) {
        alert(v1.message);
        this.isLogining = false;
      } else {
        const v2 = validate_password(this.password, '');
        if (!v2.success) {
          alert(v2.message);
          this.isLogining = false;
        } else {
          if ((!this.verificationCode) || (this.verificationCode.length === 0)) {
            alert('请输入验证码');
            this.isLogining = false;
          } else if (this.verificationCode.length !== 4) {
            alert('输入的验证码有误');
            this.isLogining = false;
          } else {

            const account = this.account;
            const password = this.http.encrypt(this.password);
            const verificationCode = this.verificationCode;

            this.http.login(account, password, verificationCode,
              (data: LoginReply) => {
                if (data) {
                  if (data.firstLogin) {
                    if (data.userType === 1) {
                      this.router.navigate([ '/modify-password-admin' ]);
                    } else if (data.userType === 3) {
                      this.router.navigate([ '/modify-password' ]);
                    } else {
                      alert('登陆失败');
                      this.reset();
                    }
                  } else {
                    if (data.userType === 0) {
                      alert('登陆失败');
                      this.reset();
                    } else if (data.userType === 1) {
                      this.router.navigate([ '/home' ]);
                    } else if (data.userType === 2) {
                      alert('登陆失败');
                      this.reset();
                    } else if (data.userType === 3) {
                      this.router.navigate([ this.http.loginPage ]);
                    } else {
                      alert('登陆失败');
                      this.reset();
                    }
                  }
                } else {
                  this.verificationCodeClicked();
                  alert('登陆失败');
                }

                this.isLogining = false;
              },
              (summary, particulars) => {
                alert(particulars);
                this.reset();

                this.isLogining = false;
              }
            );
          }
        }
      }
    }
  }

  reset() {
    this.password = '';
    this.verificationCode = '';
    this.verificationCodeClicked();
  }
}
