import { Router } from '@angular/router';
import { Component, OnInit, ElementRef, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { HttpService } from '../service/http.service';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, AfterViewInit, OnDestroy {
  isUploadImage = false;
  keypressEvent;

  @ViewChild('textInput')
  textInput: ElementRef;
  @ViewChild('imgButton')
  imgButton: ElementRef;
  @ViewChild('searchButton')
  searchButton: ElementRef;
  @ViewChild('uploadImageDiv')
  uploadImageDiv: ElementRef;
  @ViewChild('files')
  files: ElementRef;
  @ViewChild('userDiv')
  userDiv: ElementRef;

  constructor(public http: HttpService, public router: Router) {
    this.keypressEvent = (event: KeyboardEvent) => {
      if (event.keyCode === 13) {
        this.searchByText();
      }
    };
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    window.addEventListener('keypress', this.keypressEvent);
  }

  ngOnDestroy() {
    window.removeEventListener('keypress', this.keypressEvent);
  }

  popupUserContextMenu() {
    const $userDiv = $(this.userDiv.nativeElement);
    const position = $userDiv.position();
    this.http.openUserContextMenu(position.left, position.top + $userDiv.outerHeight());
  }

  login() {
    this.http.loginPage = '/search';
    this.router.navigate(['/login']);
  }

  dragoverImage(event: DragEvent) {
    event.preventDefault();
  }

  dropImage(event: DragEvent) {
    event.preventDefault();
    if (event.dataTransfer && event.dataTransfer.files && (event.dataTransfer.files.length > 0)) {
      const file = event.dataTransfer.files[0];
      const nameUpper = file.name.toUpperCase();
      if (nameUpper.endsWith('.JPG') || nameUpper.endsWith('.JPEG') || nameUpper.endsWith('.PNG')) {
        this.http.searchInfo.request.firstSearch = 1;
        this.http.setSearchImage(file);
        this.searchByImage();
      } else {
        alert('不支持的图片格式，目前只支持：jpg、jpeg和png');
      }
    }
  }

  filesSelected() {
    const file = this.files.nativeElement.files[0];
    const nameUpper = file.name.toUpperCase();
    if (nameUpper.endsWith('.JPG') || nameUpper.endsWith('.JPEG') || nameUpper.endsWith('.PNG')) {
      this.http.searchInfo.request.firstSearch = 1;
      this.http.setSearchImage(file);
      this.searchByImage();
    } else {
      alert('不支持的图片格式，目前只支持：jpg、jpeg和png');
    }
  }

  uploadImage() {
    if (!this.isUploadImage) {
      this.isUploadImage = true;
      $(this.textInput.nativeElement).removeClass('input-1').addClass('input-2').prop('placeholder', '请选择需要查找的图片').prop('readonly', true);
      $(this.imgButton.nativeElement).removeClass('img-button-1').addClass('img-button-2');
      $(this.searchButton.nativeElement).removeClass('search-button-1').addClass('search-button-2');
      $(this.uploadImageDiv.nativeElement).removeClass('upload-image-2').addClass('upload-image-1');
    } else {
      this.close();
    }
  }

  uploadLocalImage() {
    $(this.files.nativeElement).trigger('click');
  }

  close() {
    if (this.isUploadImage) {
      this.isUploadImage = false;
      $(this.textInput.nativeElement).removeClass('input-2').addClass('input-1').prop('placeholder', '请输入需要查找的文本').prop('readonly', false);
      $(this.imgButton.nativeElement).removeClass('img-button-2').addClass('img-button-1');
      $(this.searchButton.nativeElement).removeClass('search-button-2').addClass('search-button-1');
      $(this.uploadImageDiv.nativeElement).removeClass('upload-image-1').addClass('upload-image-2');
    }
  }

  searchByText() {
    if (this.http.searchInfo.request.text && this.http.searchInfo.request.text.length > 0) {
      this.http.searchInfo.request.firstSearch = 1;
      this.http.searchInfo.isByText = true;
      if (this.http.isExtifsMode) {
        this.router.navigateByUrl(`/extifs/search-result?account=${this.http.authInfo.account}&username=${this.http.authInfo.username}&secretKey=${this.http.authInfo.secretKey}`);
      } else {
        this.router.navigate([ '/search-result' ]);
      }
    } else {
      alert('请输入需要检索的关键字');
    }
  }

  searchByImage() {
    this.http.searchInfo.isByText = false;
    if (this.http.isExtifsMode) {
      this.router.navigateByUrl(`/extifs/search-result?account=${this.http.authInfo.account}&username=${this.http.authInfo.username}&secretKey=${this.http.authInfo.secretKey}`);
    } else {
      this.router.navigate([ '/search-result' ]);
    }
  }
}
