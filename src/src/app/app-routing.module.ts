import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SearchComponent } from './search/search.component';
import { AuthenticatedGuard } from './service/authenticated-guard.service';
import { ResultComponent } from './result/result.component';
import { ModifyPasswordComponent } from './modify-password/modify-password.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { LogManagementComponent } from './log-management/log-management.component';
import { HomeComponent } from './home/home.component';
import { ModifyPasswordAdminComponent } from './modify-password-admin/modify-password-admin.component';
import { ExtifsSearchComponent } from './extifs-search/extifs-search.component';
import { ExtifsSearchResultComponent } from './extifs-search-result/extifs-search-result.component';
import { VideoTagsManagementComponent } from './video-tags-management/video-tags-management.component';
import { FrameTagsManagementComponent } from './frame-tags-management/frame-tags-management.component';


const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'search', component: SearchComponent, canActivate: [AuthenticatedGuard] },
  { path: 'search-result', component: ResultComponent, canActivate: [AuthenticatedGuard] },
  { path: 'modify-password', component: ModifyPasswordComponent, canActivate: [AuthenticatedGuard] },
  { path: 'modify-password-admin', component: ModifyPasswordAdminComponent, canActivate: [AuthenticatedGuard] },
  { path: 'home', component: HomeComponent,
    children : [
      { path: '', redirectTo: 'user-management', pathMatch: 'full' },
      { path: 'user-management', component: UserManagementComponent, canActivate: [AuthenticatedGuard] },
      { path: 'log-management', component: LogManagementComponent, canActivate: [AuthenticatedGuard] },
      { path: 'video-tags-management', component: VideoTagsManagementComponent, canActivate: [AuthenticatedGuard] },
      { path: 'frame-tags-management', component: FrameTagsManagementComponent, canActivate: [AuthenticatedGuard]}
    ]
  },

  // /extifs/search?account=account&username=username&secretKey=secretKey
  { path: 'extifs/search', component: ExtifsSearchComponent },
  { path: 'extifs/search-result', component: ExtifsSearchResultComponent }
];

/*
const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'search', component: SearchComponent },
  { path: 'search-result', component: ResultComponent },
  { path: 'modify-password', component: ModifyPasswordComponent },
  { path: 'modify-password-admin', component: ModifyPasswordAdminComponent },
  { path: 'home', component: HomeComponent,
    children : [
      { path: '', redirectTo: 'user-management', pathMatch: 'full' },
      { path: 'user-management', component: UserManagementComponent },
      { path: 'log-management', component: LogManagementComponent },
      { path: 'video-tags-management', component: VideoTagsManagementComponent },
      { path: 'frame-tags-management', component: FrameTagsManagementComponent}
    ]
  },

  // /extifs/search?account=account&username=username&secretKey=secretKey
  { path: 'extifs/search', component: ExtifsSearchComponent },
  { path: 'extifs/search-result', component: ExtifsSearchResultComponent }
];
*/

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
