import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyPasswordAdminComponent } from './modify-password-admin.component';

describe('ModifyPasswordAdminComponent', () => {
  let component: ModifyPasswordAdminComponent;
  let fixture: ComponentFixture<ModifyPasswordAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyPasswordAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyPasswordAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
