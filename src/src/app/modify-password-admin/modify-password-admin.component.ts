import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { HttpService } from '../service/http.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Validator, validate_username, validate_password } from '../common/validator';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-modify-password-admin',
  templateUrl: './modify-password-admin.component.html',
  styleUrls: ['./modify-password-admin.component.css']
})
export class ModifyPasswordAdminComponent implements OnInit, OnDestroy {
  @ViewChild('userDiv')
  userDiv: ElementRef;

  // subscribeParams: Subscription;
  // firstLogin: boolean;

  username: string;
  oldPassword: string;
  newPassword: string;
  newPasswordConfirm: string;

  validators: Validator[];
  canSave: boolean;

  constructor(public http: HttpService, public router: Router, public route: ActivatedRoute) {
    this.reset();
    this.validators = [
      new Validator(
        (): { success: boolean, message: string } => {
          this.username = this.username.trim();
          return validate_username(this.username);
        }
      ),
      new Validator(
        (): { success: boolean, message: string } => {
          return validate_password(this.oldPassword, '原始');
        }
      ),
      new Validator(
        (): { success: boolean, message: string } => {
          return validate_password(this.newPassword, '新');
        }
      ),
      new Validator(
        (): { success: boolean, message: string } => {
          if (this.newPasswordConfirm.length === 0) {
            return {
              success: false,
              message: '确认密码不能为空'
            };
          } else if (this.newPassword !== this.newPasswordConfirm) {
            return {
              success: false,
              message: '新密码与确认密码不一致'
            };
          }
          return {
            success: true,
            message: ''
          };
        }
      )
    ];
  }

  ngOnInit() {
    /*
    this.subscribeParams = this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.firstLogin = params.has('firstLogin');
      }
    );
    */
  }

  ngOnDestroy() {
    // this.subscribeParams.unsubscribe();
  }

  usernameValidate(): Validator {
    return this.validators[0];
  }

  oldPasswordValidate(): Validator {
    return this.validators[1];
  }

  newPasswordValidate(): Validator {
    return this.validators[2];
  }

  newPasswordConfirmValidate(): Validator {
    return this.validators[3];
  }

  validate() {
    this.canSave = true;
    for (let i = 0; i < this.validators.length; ++i) {
      const result = this.validators[i].validate(true);
      if (!result.success) {
        this.canSave = false;
        alert(result.message);
        break;
      }
    }
  }

  popupUserContextMenu() {
    const $userDiv = $(this.userDiv.nativeElement);
    const position = $userDiv.position();
    this.http.openUserContextMenu(position.left, position.top + $userDiv.outerHeight());
  }

  save() {
    this.validate();
    if (this.canSave) {
      this.http.modifyUserPassword(
        this.http.authInfo.userId, this.http.authInfo.username, this.username, this.http.encrypt(this.oldPassword), this.http.encrypt(this.newPassword),
        () => {
          confirm('修改用户密码成功!');
          this.http.logout();
        },
        (summary, particulars) => {
          alert(particulars);
          this.reset();
        }
      );
    }
  }

  reset() {
    if (this.http.authInfo.username) {
      this.username = this.http.authInfo.username;
    } else {
      this.username = '';
    }
    this.oldPassword = '';
    this.newPassword = '';
    this.newPasswordConfirm = '';
  }
}
