import { Router } from '@angular/router';
import { Component, AfterViewInit, ViewChild, ElementRef, HostListener, OnDestroy } from '@angular/core';
import { HttpService } from '../service/http.service';
import { MediaSearch } from '../common/media-search-reply';
import { toDescriptionShow, toMediaSecondShow } from '../common/reply';

declare const $: JQueryStatic;

@Component({
  selector: 'mms-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements AfterViewInit, OnDestroy {
  isUploadImage = false;
  isShowByList = true;

  @ViewChild('byText')
  byText: ElementRef;
  @ViewChild('byImage')
  byImage: ElementRef;
  @ViewChild('textInput')
  textInput: ElementRef;
  @ViewChild('imagePreview')
  imagePreview: ElementRef;
  @ViewChild('imageNameInput')
  imageNameInput: ElementRef;
  @ViewChild('imageCloseButton')
  imageCloseButton: ElementRef;
  @ViewChild('imgUploadDropDownButton')
  imgUploadDropDownButton: ElementRef;
  @ViewChild('imgSearchButton')
  imgSearchButton: ElementRef;
  @ViewChild('imgButton')
  imgButton: ElementRef;
  @ViewChild('searchButton')
  searchButton: ElementRef;
  @ViewChild('results')
  results: ElementRef;
  @ViewChild('listE')
  listE: ElementRef;
  @ViewChild('gridE')
  gridE: ElementRef;
  @ViewChild('userDiv')
  userDiv: ElementRef;

  $popup: JQuery<HTMLElement>;
  $resizeFn;
  $keypressFn;

  constructor(public http: HttpService, private router: Router) {
    this.$resizeFn = () => {
      this.closePopup();
    };
    this.$keypressFn = (event: KeyboardEvent) => {
      if (event.keyCode === 13) {
        if (this.http.searchInfo.isByText) {
          this.searchByText();
        } else {
          this.searchByImage();
        }
      }
    };

    if (!this.http.authInfo.isAuthenticated) {
      this.http.searchInfo.reply.course.list = [
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学', courseCoverCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 0, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '图书', courseName: '高等数学', courseCoverCdn: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=86877019,1987543012&fm=27&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '图书', mediaSecond: 0, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 200, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2012156866,3371681657&fm=200&gp=0.jpg', courseDescription: '', mediaUrlId: '', mediaType: '视频', mediaSecond: 0, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3356756675,2857832266&fm=27&gp=0.jpg', courseDescription: '', mediaUrlId: '', mediaType: '视频', mediaSecond: 10, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3218731201,4227962901&fm=200&gp=0.jpg', courseDescription: '', mediaUrlId: '', mediaType: '视频', mediaSecond: 100, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '', mediaUrlId: '', mediaType: '视频', mediaSecond: 1000, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '文档', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '文档', mediaSecond: 0, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '不显示画面位置', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '', mediaUrlId: '', mediaType: '视频', mediaSecond: null, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '', mediaUrlId: '', mediaType: '视频', mediaSecond: 10, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 200, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 200, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 200, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 200, mediaSecondShow: '', courseDescriptionShow: '' },
        { courseId: 'courseId', courseUrlId: '', courseType: '视频', courseName: '高等数学', courseCoverCdn: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2369209182,3501169783&fm=200&gp=0.jpg', courseDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaUrlId: '', mediaType: '视频', mediaSecond: 200, mediaSecondShow: '', courseDescriptionShow: '' }
      ];
      this.http.splitSearchReply(this.http.searchInfo.reply.course.list, this.http.searchInfo.reply.course.grid,
        (item: MediaSearch.Course) => {
          item.courseDescriptionShow = toDescriptionShow(item.courseDescription);
          if (item.mediaSecond || (item.mediaSecond === 0)) {
            item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
          }
        }
      );

      this.http.searchInfo.reply.resource.list = [
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学高等数学', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 0, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '图书', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 100, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '网络课程', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 200, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '文档', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 300, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 400, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '图书', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 500, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '网络课程', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 600, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '文档', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 700, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '网络课程', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 800, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 900, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 1000, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 1000, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 1000, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 1000, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 1000, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' },
        { imageId: '', imageType: 'video', imageCdn: 'https://f10.baidu.com/it/u=228848094,1759153097&fm=72', mediaId: '', mediaUrlId: '', mediaType: '视频', mediaName: '测试使用', mediaCdn: '', mediaCoverCdn: '', mediaSecond: 1000, mediaDescription: '关于2中题主所述字体转换网站或工具不支持中文字体转换，这个观点是在网上得到的，在实践中题主遇到过这样一种情况，同一种中文字体，例如最近使用过的思源黑体，在网上下载不同的思源黑体字体文件，有的能用2中所述两个网站转换，有的不能，(即使转换出来得到了字体，显示上也不尽如人意)。因此无法对是否支持中文字体转换下结论，有没有大神可以给个结论？', mediaSecondShow: '', mediaDescriptionShow: '' }
      ];
      this.http.splitSearchReply(this.http.searchInfo.reply.resource.list, this.http.searchInfo.reply.resource.grid,
        (item: MediaSearch.Resource) => {
          item.mediaDescriptionShow = toDescriptionShow(item.mediaDescription);
          if (item.mediaSecond || (item.mediaSecond === 0)) {
            item.mediaSecondShow = toMediaSecondShow(item.mediaSecond);
          }
        }
      );
    }

  }

  @HostListener('click', ['$event.target'])
  onClicked(target: HTMLElement) {
    if (!this.byText.nativeElement.contains(target) && !this.byImage.nativeElement.contains(target)) {
      this.closePopup();
    }
  }

  ngAfterViewInit() {
    window.addEventListener('resize', this.$resizeFn);
    window.addEventListener('keypress', this.$keypressFn);
    this.http.searchInfo.request.firstSearch = 1;
    this.http.search(1);
    if (!this.http.searchInfo.isByText) {
      $(this.byText.nativeElement).addClass('display-none');
      $(this.byImage.nativeElement).removeClass('display-none');
      this.preview();
    }
  }

  ngOnDestroy() {
    window.removeEventListener('resize', this.$resizeFn);
    window.removeEventListener('keypress', this.$keypressFn);
  }

  uploadImageDropDown() {
    const $textInput = $(this.textInput.nativeElement);
    const $imagePreview = $(this.imagePreview.nativeElement);
    const $imageNameInput = $(this.imageNameInput.nativeElement);
    const $imageCloseButton = $(this.imageCloseButton.nativeElement);
    const $imgUploadDropDownButton = $(this.imgUploadDropDownButton.nativeElement);
    const $imgButton = $(this.imgButton.nativeElement);
    const $searchButton = $(this.searchButton.nativeElement);
    if (!this.isUploadImage) {
      this.isUploadImage = true;
      $textInput.removeClass('input-1').addClass('input-2').prop('placeholder', '请选择需要查找的图片').prop('readonly', true);
      $imagePreview.removeClass('image-preview-1').addClass('image-preview-2');
      this.$popup = $('<div></div>')
                    .addClass('upload-image-dialog')
                    .css('left', Math.floor(this.http.searchInfo.isByText ? $textInput.position().left : $imagePreview.position().left))
                    .css('top', this.http.searchInfo.isByText ? ($textInput.position().top + $textInput.outerHeight() - 2) : ($imagePreview.position().top + $imagePreview.outerHeight() - 2))
                    .css('width',
                      this.http.searchInfo.isByText
                        ? ($textInput.outerWidth() + $imgButton.outerWidth())
                          : ($imagePreview.outerWidth() + $imageNameInput.outerWidth() + $imageCloseButton.outerWidth() + $imgUploadDropDownButton.outerWidth()))
                    .css('height', 186);
      $('<div></div>')
        .addClass('column center-c upload-image-dialog-rectangle')
        .append($('<img width="36" height="34" src="assets/images/search_pic_add.png">'))
        .append($('<div style="color: rgb(199, 199, 199); font-size: 1.6em; font-weight: bold; ">拖拽图片到这里</div>'))
        .on('dragover',
          (event) => {
            event.preventDefault();
          }
        )
        .on('drop',
          (event) => {
            event.preventDefault();
            try {
              const dragEvent: DragEvent = event.originalEvent as DragEvent;
              if (dragEvent.dataTransfer && dragEvent.dataTransfer.files && (dragEvent.dataTransfer.files.length > 0)) {
                const ifile = dragEvent.dataTransfer.files[0];
                const nameUpper = ifile.name.toUpperCase();
                if (nameUpper.endsWith('.JPG') || nameUpper.endsWith('.JPEG') || nameUpper.endsWith('.PNG')) {
                  if (this.http.searchInfo.isByText) {
                    this.http.searchInfo.isByText = false;
                    $(this.byText.nativeElement).addClass('display-none');
                    $(this.byImage.nativeElement).removeClass('display-none');
                  }

                  this.http.searchInfo.request.firstSearch = 1;
                  this.http.setSearchImage(ifile);
                  this.http.searchByImage(1);
                  this.closePopup();
                  this.preview();
                } else {
                  alert('不支持的图片格式，目前只支持：jpg、jpeg和png');
                }
              }
            } catch (e) {
              console.log(e);
            }
          }
        )
        .appendTo(this.$popup);
      const file = $('<input class="upload-image-file" type="file"/>');
        file.on('change',
          () => {
            const ele: any = file[0];
            const ifile = ele.files[0];
            const nameUpper = ifile.name.toUpperCase();
            if (nameUpper.endsWith('.JPG') || nameUpper.endsWith('.JPEG') || nameUpper.endsWith('.PNG')) {
              if (this.http.searchInfo.isByText) {
                this.http.searchInfo.isByText = false;
                $(this.byText.nativeElement).addClass('display-none');
                $(this.byImage.nativeElement).removeClass('display-none');
              }

              this.http.searchInfo.request.firstSearch = 1;
              this.http.setSearchImage(ifile);
              this.http.searchByImage(1);
              this.closePopup();
              this.preview();
            } else {
              alert('不支持的图片格式，目前只支持：jpg、jpeg和png');
            }
          }
        );
      $('<div></div>')
        .addClass('row-0 center-m')
        .append(file)
        .append($('<div class="upload-image-button"></div>')
          .on('click',
            () => {
              file.trigger('click');
            }
          )
        )
        .appendTo(this.$popup);
      $('<div style="height: 0; position: relative;"></div>')
        .addClass('row-0 right-m')
        .append($('<button class="upload-image-close-button"></button>')
          .on('click',
            () => {
              this.closePopup();
            }
          )
        )
        .append('<div style="margin-left: 15px;"></div>')
        .appendTo(this.$popup);
      $('<div class="spacer-2"></div>')
        .appendTo(this.$popup);
      $(document.body).append(this.$popup);
    } else {
      this.closePopup();
    }
  }

  closePopup() {
    if (this.isUploadImage) {
      this.isUploadImage = false;
      $(this.textInput.nativeElement).removeClass('input-2').addClass('input-1').prop('placeholder', '请输入需要查找的文本').prop('readonly', false);
      $(this.imagePreview.nativeElement).removeClass('image-preview-2').addClass('image-preview-1');
      this.$popup.remove();
      this.$popup = null;
    }
  }

  preview() {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(this.http.searchInfo.request.image);
    fileReader.onload = (event: any) => {
      $(this.imagePreview.nativeElement).prop('src', event.target.result);
    };
  }

  closeImageSearch() {
    this.closePopup();
    this.http.searchInfo.isByText = true;
    $(this.byText.nativeElement).removeClass('display-none');
    $(this.byImage.nativeElement).addClass('display-none');
    this.http.setSearchImage(null);
  }

  searchByText() {
    if (this.isUploadImage) {
      alert('请选择需要检索的图片');
    } else if (this.http.searchInfo.request.text && this.http.searchInfo.request.text.length > 0) {
      if (this.http.searchInfo.request.prevText !== this.http.searchInfo.request.text) {
        this.http.searchInfo.request.prevText = this.http.searchInfo.request.text;
        this.http.searchInfo.request.firstSearch = 1;
      } else {
        this.http.searchInfo.request.firstSearch = 0;
      }

      this.http.searchByText(1);
      this.closePopup();
    } else {
      alert('请输入需要检索的关键字');
    }
  }

  searchByImage() {
    if (this.http.searchInfo.request.image) {
      this.http.searchInfo.request.firstSearch = 0;
      this.http.searchByImage(1);
      this.closePopup();
    } else {
      alert('请选择需要检索的图片');
    }
  }

  list() {
    if (!this.isShowByList) {
      this.isShowByList = true;
      $(this.listE.nativeElement).removeClass('list-2').addClass('list-1');
      $(this.gridE.nativeElement).removeClass('grid-2').addClass('grid-1');
    }
  }

  grid() {
    if (this.isShowByList) {
      this.isShowByList = false;
      $(this.listE.nativeElement).removeClass('list-1').addClass('list-2');
      $(this.gridE.nativeElement).removeClass('grid-1').addClass('grid-2');
    }
  }

  filterChanged(mediaType: number) {
    if (this.http.searchInfo.request.mediaType !== mediaType) {
      this.http.searchInfo.request.firstSearch = 1;
      this.http.searchInfo.request.mediaType = mediaType;
      this.http.searchInfo.reply.page = null;
      if (this.http.authInfo.isAuthenticated) {
        this.http.clearMediaSearchReply();
        this.http.search(1);
      }
    }
  }

  currentPageChanged(currentPage) {
    this.http.searchInfo.request.firstSearch = 0;
    this.http.search(currentPage);
  }

  popupUserContextMenu() {
    if (!this.http.isExtifsMode) {
      const $userDiv = $(this.userDiv.nativeElement);
      const position = $userDiv.position();
      this.http.openUserContextMenu(position.left, position.top + $userDiv.outerHeight());
    }
  }

  login() {
    this.http.loginPage = '/search-result';
    this.router.navigate(['/login']);
  }

  gotoHome() {
    if (this.http.isExtifsMode) {
      this.router.navigateByUrl(`/extifs/search?account=${this.http.authInfo.account}&username=${this.http.authInfo.username}&secretKey=${this.http.authInfo.secretKey}`);
    } else {
      this.router.navigate(['/search']);
    }
  }
}
