import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mms-pagination-button',
  templateUrl: './pagination-button.component.html',
  styleUrls: ['./pagination-button.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PaginationButtonComponent implements OnInit {

  constructor(public element: ElementRef) { }

  ngOnInit() {
  }

}
