import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagementDeleteUserComponent } from './user-management-delete-user.component';

describe('UserManagementDeleteUserComponent', () => {
  let component: UserManagementDeleteUserComponent;
  let fixture: ComponentFixture<UserManagementDeleteUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserManagementDeleteUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserManagementDeleteUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
