import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../service/http.service';

@Component({
  selector: 'mms-user-management-delete-user',
  templateUrl: './user-management-delete-user.component.html',
  styleUrls: ['./user-management-delete-user.component.css']
})
export class UserManagementDeleteUserComponent implements OnInit {
  @Input() params: { accounts: string[], ok: () => void };

  constructor(public http: HttpService) { }

  ngOnInit() {
  }

  getAccount(): string {
    if (this.params && this.params.accounts && this.params.accounts.length > 0) {
      if (this.params.accounts.length > 1) {
        return `${this.params.accounts[0]}, ...`;
      }
      return this.params.accounts[0];
    }
    return '';
  }

  closeDialog() {
    this.http.showDeleteUserDialog = false;
  }

  ok() {
      this.closeDialog();
      if (this.params && this.params.ok) {
        this.params.ok();
      }
  }
  cancel() {
    this.closeDialog();
  }
}
