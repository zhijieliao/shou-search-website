import { Component, AfterViewInit, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../service/http.service';
import { SearchHistoryService } from '../service/search-history.service';
import { TagSearch } from '../common/tag-search-reply';

import { enableProdMode } from '@angular/core';
enableProdMode();

declare const $: any;

interface FramePos {
  x: number;
  y: number;
}

@Component({
  selector: 'mms-frame-tags-management',
  templateUrl: './frame-tags-management.component.html',
  styleUrls: ['./frame-tags-management.component.css']
})

export class FrameTagsManagementComponent implements AfterViewInit,  OnInit {
  $window: {
    width: number;
    height: number
  };
  selectedTags = '';
  beginTime = '';
  endTime = '';

  hourNumbers: number[] = [];
  msNumbers: number[] = [];

  selectedTagsText = '全部片段标签';
  isSelectDlgShow = false;

  canResetCondition = false;

  frameTagSearchReply: TagSearch.VideoFrame = { video: {mediaId: '', mediaUrlId: '', mediaType: '', mediaName: '', framesTagged: true}, frames: []};
  showFrameTags: TagSearch.Frame[][] = [];

  selectedFramesPos: string[] = [];
  selectedFrames: TagSearch.Frame[] = [];
  defaultFramePos: FramePos = { x: -1, y: -1};

  overviewTagsFrame: TagSearch.Frame;

  isConfirmDlgShow = false;
  confirmDlgTittle = '';
  confirmDlgText = '';
  removeImageId = '';
  removeTag = '';
  nextDefaultFramePos: FramePos = { x: -1, y: -1};

  isInformationDlgShow = false;
  informationDlgText = '';

  constructor(public http: HttpService, private router: Router, private searchHistory: SearchHistoryService) {
    this.$window = { width: window.innerWidth, height: window.innerHeight };
    window.addEventListener('resize',
      () => {
        this.$window.width = window.innerWidth;
        this.$window.height = window.innerHeight;
      }
    );
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getDefaultFrame();

    this.selectedTags = this.searchHistory.frameTag.selectedTags;
    this.selectedTagsText = this.selectedTags.length > 0 ?
                '已选择 ' + this.selectedTags.split(',').length + ' 个片段标签' : '全部片段标签';
    this.beginTime = this.searchHistory.frameTag.beginTime;
    this.endTime = this.searchHistory.frameTag.endTime;
    this.searchFramesByInput();

    if (this.http.homeViewUrlChange) {
      this.http.homeViewUrlChange(this.router.url);
    }

    this.checkSearchConditionChanged();
  }

  checkSearchConditionChanged() {
    if (this.beginTime.length > 0 || this.endTime.length > 0 || this.selectedTags.length > 0) {
      this.canResetCondition = true;
    } else {
      this.canResetCondition = false;
    }
  }

  resetCondition() {
    this.selectedTags = '';
    this.beginTime = '';
    this.endTime = '';
    this.selectedTagsText = '全部片段标签';
    this.canResetCondition = false;
  }

  beginTimeUpdate(newTime: string) {
    this.beginTime = newTime;
    this.checkSearchConditionChanged();
  }

  endTimeUpdate(newTime: string) {
    this.endTime = newTime;
    this.checkSearchConditionChanged();
  }

  getDefaultFrame() {
    for ( let i = 0; i < this.showFrameTags.length; ++i) {
      for ( let j = 0 ; j < this.showFrameTags[i].length; ++j) {
        if (this.showFrameTags[i][j].isDefaultFrame) {
          this.defaultFramePos = { x: i, y: j};
        }
      }
    }
  }

  parseInt(s: string, radix?: number): number {
    const number = parseInt(s, 10);
    return isNaN(number) ? 0 : number;
  }

  timeToSecond(time: string): number {
    const timeArr = time.split(':');
    return this.parseInt(timeArr[0], 10) * 3600 + this.parseInt(timeArr[1], 10) * 60 + this.parseInt(timeArr[2], 10);
  }

  secondToTime(totalSecond: number): string {
    const hour = Math.floor(totalSecond / 3600);
    const minute = Math.floor((totalSecond % 3600) / 60);
    const second = (totalSecond % 3600) % 60;

    const hourStr = hour > 9 ? hour : '0' + hour;
    const minuteStr = minute > 9 ? minute : '0' + minute;
    const secondStr = second > 9 ? second : '0' + second;
    return hourStr + ':' + minuteStr + ':' + secondStr;
  }

  updateVideoStatus() {
    this.http.updateVideoStatus(this.frameTagSearchReply.video.mediaId, !this.frameTagSearchReply.video.framesTagged,
      () => {
        this.frameTagSearchReply.video.framesTagged = !this.frameTagSearchReply.video.framesTagged;
      },
      () => {}
    );
  }

  searchFramesByHistory() {
    this.searchFrames(this.searchHistory.frameTag.selectedTags,
      this.searchHistory.frameTag.beginTime, this.searchHistory.frameTag.endTime);
  }

  searchFramesByInput() {
    this.searchFrames(this.selectedTags, this.beginTime, this.endTime);
  }

  searchFrames(selectedTags: string, beginTime: string, endTime: string ) {
    let beginSecond = null;
    let endSecond = null;
    if (beginTime.length > 0) {
      beginSecond = this.timeToSecond(beginTime);
    }
    if (endTime.length > 0) {
      endSecond = this.timeToSecond(endTime);
      if (beginTime.length > 0 && endSecond < beginSecond) {
        this.openInformationDlg('开始时间不能大于结束时间!');
        return;
      }
    }

    this.http.searchVideoFrame(this.searchHistory.frameTag.mediaId, selectedTags,
      beginSecond, endSecond,
      (data: TagSearch.VideoFrame) => {
        this.frameTagSearchReply = data;
        this.showFrameTags = [];
        this.defaultFramePos = { x: -1, y: -1};
        this.nextDefaultFramePos = { x: -1, y: -1};

        const overviewImageId = this.overviewTagsFrame ? this.overviewTagsFrame.imageId : undefined;
        this.overviewTagsFrame = undefined;
        let tmp: any[];
        for (let i = 0; i < this.frameTagSearchReply.frames.length; ++i) {
          if ((i % 4) === 0) {
            tmp = [ ];
            this.showFrameTags.push(tmp);
          }

          const frame = this.frameTagSearchReply.frames[i];
          frame.tagArray = frame.tags.length === 0 ? [] : frame.tags.split(',');
          tmp.push(frame);
          if (overviewImageId && frame.imageId === overviewImageId) {
            this.overviewTagsFrame = frame;
          }
        }

        this.getDefaultFrame();
        this.searchHistory.frameTag = {
          mediaId: this.searchHistory.frameTag.mediaId,
          selectedTags: selectedTags,
          beginTime: beginTime,
          endTime: endTime
        };
        this.searchHistory.saveFrameTagCondition();
      },
      () => {}
    );
  }

  changeFrameSeleted(frame: TagSearch.Frame) {
    const pos = this.selectedFramesPos.indexOf(frame.imageId);
    if (pos === -1) {
      this.selectedFramesPos.push(frame.imageId);
    } else {
      this.selectedFramesPos.splice(pos, 1);
    }
  }

  isFrameSeleted(frame: TagSearch.Frame): boolean {
    return this.selectedFramesPos.indexOf(frame.imageId) !== -1;
  }

  allSelection() {
    for ( let i = 0; i < this.showFrameTags.length; ++i) {
      const columnFrames = this.showFrameTags[i];
      for (let j = 0; j < columnFrames.length; ++j) {
        const pos = this.selectedFramesPos.indexOf(columnFrames[j].imageId);
        if (pos === -1) {
          this.selectedFramesPos.push(columnFrames[j].imageId);
        }
      }
    }
  }

  invertSelection() {
    for ( let i = 0; i < this.showFrameTags.length; ++i) {
      const columnFrames = this.showFrameTags[i];
      for (let j = 0; j < columnFrames.length; ++j) {
        this.changeFrameSeleted(columnFrames[j]);
      }
    }
  }

  goBack() {
    this.router.navigate(['/home/video-tags-management']);
  }

  selectTags() {
    this.isSelectDlgShow = true;
  }
  selectTagsDlgClose() {
    this.isSelectDlgShow = false;
  }
  selectedTagsChange( selectedTags: string ) {
    this.selectedTags = selectedTags;
    this.checkSearchConditionChanged();
    this.selectedTagsText = this.selectedTags.length > 0 ?
                '已选择 ' + this.selectedTags.split(',').length + ' 个片段标签' : '全部片段标签';
    this.searchHistory.updateFrameHistoryTags(this.selectedTags.split(','));
    this.isSelectDlgShow = false;
  }

  updateDefaultFrame(row: number, column: number) {
    if (row !== this.defaultFramePos.x || column !== this.defaultFramePos.y) {
      this.isConfirmDlgShow = true;
      this.confirmDlgTittle = '更改默认片段';
      this.confirmDlgText = '确定更改默认片段?';
      this.nextDefaultFramePos = {x: row, y: column};
    }
  }

  isDefaultFrame(row: number, column: number): boolean {
    return this.defaultFramePos.x === row && this.defaultFramePos.y === column;
  }

  overviewTagsDlgOpen(frame: TagSearch.Frame) {
    this.overviewTagsFrame = frame;
  }
  overviewTagsDlgClose() {
    this.overviewTagsFrame = undefined;
  }

  batchTagsDlgOpen() {
    if (this.selectedFramesPos.length === 0) {
      this.openInformationDlg('请选择您需要管理的片段!');
      return;
    }
    for ( const pos of this.selectedFramesPos) {
      for ( let i = 0; i < this.showFrameTags.length; ++i) {
        const columnFrames = this.showFrameTags[i];
        for (let j = 0; j < columnFrames.length; ++j) {
          if (columnFrames[j].imageId === pos) {
            this.selectedFrames.push(columnFrames[j]);
          }
        }
      }
    }
  }
  batchTagsDlgClose() {
    this.selectedFrames = [];
  }

  gotoVideoDetail(mediaSecond?: number) {
    if (mediaSecond) {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + this.frameTagSearchReply.video.mediaUrlId +
                  '&second=' + mediaSecond);
    } else {
      window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + this.frameTagSearchReply.video.mediaUrlId);
    }
  }

  removeFrameOneTag(imageId: string, tag: string) {
    this.isConfirmDlgShow = true;
    this.confirmDlgTittle = '删除标签';
    this.confirmDlgText = '确定删除标签?';
    this.removeImageId = imageId;
    this.removeTag = tag;
  }

  confirmDlgClose(confirm: boolean) {
    this.isConfirmDlgShow = false;

    if (confirm) {
      if (this.removeTag.length > 0) {
        this.http.removeImagesTags(this.frameTagSearchReply.video.mediaId, this.removeImageId, this.removeTag,
          () => {
            setTimeout(() => { this.searchFramesByHistory(); }, 1000);
            this.openInformationDlg('标签删除成功!');
          },
          (summary: string, particulars: string) => {
            this.openInformationDlg('标签删除失败,失败原因:' + particulars);
          }
        );
        this.removeTag = '';
      } else if (this.nextDefaultFramePos.x >= 0) {
        const frame = this.showFrameTags[this.nextDefaultFramePos.x][this.nextDefaultFramePos.y];
        this.http.updateVideoDefaultFrame(this.frameTagSearchReply.video.mediaId, frame.imageId,
          () => { this.defaultFramePos = this.nextDefaultFramePos; this.nextDefaultFramePos = { x: -1, y: -1}; },
          () => { this.nextDefaultFramePos = { x: -1, y: -1}; }
        );
      }
    }
  }

  openInformationDlg(text: string) {
    this.isInformationDlgShow = true;
    this.informationDlgText = text;
  }

  closeInformationDlg() {
    this.isInformationDlgShow = false;
    this.informationDlgText = '';
  }
}
