import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameTagsManagementComponent } from './frame-tags-management.component';

describe('FrameTagsManagementComponent', () => {
  let component: FrameTagsManagementComponent;
  let fixture: ComponentFixture<FrameTagsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameTagsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameTagsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
