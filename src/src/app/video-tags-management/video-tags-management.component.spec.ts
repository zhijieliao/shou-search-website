import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoTagsManagementComponent } from './video-tags-management.component';

describe('VideoTagsManagementComponent', () => {
  let component: VideoTagsManagementComponent;
  let fixture: ComponentFixture<VideoTagsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoTagsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoTagsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
