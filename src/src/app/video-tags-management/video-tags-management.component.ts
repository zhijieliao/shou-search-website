import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../service/http.service';
import { SearchHistoryService } from '../service/search-history.service';
import { PageReply } from '../common/page';
import { TagSearch } from '../common/tag-search-reply';

declare const $: any;

@Component({
  selector: 'mms-video-tags-management',
  templateUrl: './video-tags-management.component.html',
  styleUrls: ['./video-tags-management.component.css']
})
export class VideoTagsManagementComponent implements AfterViewInit, OnInit {
  $window: {
    width: number;
    height: number
  };
  mediaName = '';
  selectedTags = '';
  framesTagged = '0';
  selectedTagsText = '全部视频标签';

  perPageTotalCount: number;
  page: PageReply = { begin: 1, current: 1, end: 0, totalCount: 0 };
  videos: TagSearch.Video[] = [];

  overviewVideo: TagSearch.Video;
  isSelectDlgShow = false;

  canResetCondition = false;

  isInformationDlgShow = false;
  informationDlgText = '';

  constructor(private http: HttpService, private router: Router, private searchHistory: SearchHistoryService) {
    this.$window = { width: window.innerWidth, height: window.innerHeight };
    window.addEventListener('resize',
      () => {
        this.$window.width = window.innerWidth;
        this.$window.height = window.innerHeight;
      }
    );

    const win: any = window;
    if (win.configs) {
      this.perPageTotalCount = win.configs.videoTagInfoPerPageNum;
    } else {
      this.perPageTotalCount = 10;
    }

    this.updateSelectedTags();

    // this.videos = [
    //   { mediaId: '1', mediaUrlId: 'ddddd', mediaType: '视频', mediaName: '测试视频', mediaDefaultFrameCdn: '',
    //   mediaCreateTime: '2018-05-10 15:59:00', tags: '标签1,标签2,标签3,标签4,标签5,标签6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', framesTagged: true},
    //   { mediaId: '2', mediaUrlId: 'ddddd', mediaType: '视频', mediaName: '测试视频', mediaDefaultFrameCdn: '',
    //   mediaCreateTime: '2018-05-10 15:59:00', tags: '标签1,标签标签标签标签标签2,标签3,标签4,标签5', framesTagged: false},
    //   { mediaId: '3', mediaUrlId: 'ddddd', mediaType: '视频', mediaName: '测试视频', mediaDefaultFrameCdn: '',
    //   mediaCreateTime: '2018-05-10 15:59:00', tags: '标签1,标签标签标签标签标签2,标签3,标签4', framesTagged: true},
    //   { mediaId: '4', mediaUrlId: 'ddddd', mediaType: '视频', mediaName: '测试视频', mediaDefaultFrameCdn: '',
    //   mediaCreateTime: '2018-05-10 15:59:00', tags: '标签1,标签2', framesTagged: false}
    // ];
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (this.http.homeViewUrlChange) {
      this.http.homeViewUrlChange(this.router.url);
    }

    this.mediaName = this.searchHistory.videoTag.mediaName;
    this.selectedTags = this.searchHistory.videoTag.selectedTags;
    this.updateSelectedTags();
    this.framesTagged = this.searchHistory.videoTag.framesTagged;
    const beginTimeInput = $('#beginTimeInput');
    beginTimeInput.dcalendarpicker({ initDate: this.searchHistory.videoTag.beginTime, canClear: true });
    const endTimeInput = $('#endTimeInput');
    endTimeInput.dcalendarpicker({ initDate: this.searchHistory.videoTag.endTime, canClear: true });
    if (this.http.authInfo.isAuthenticated) {
      this.currentPageChanged(this.searchHistory.videoTag.currentPage);
    }

    beginTimeInput.on('dateselected', (e) => {
      this.checkSearchConditionChanged();
    });
    endTimeInput.on('dateselected', (e) => {
      this.checkSearchConditionChanged();
    });

    this.checkSearchConditionChanged();
  }

  checkSearchConditionChanged(mediaName?: string) {
    if (!mediaName) {
      mediaName = this.mediaName;
    }
    if ($('#beginTimeInput').val().length > 0 || $('#endTimeInput').val().length > 0 ||
        mediaName.length > 0 || this.selectedTags.length > 0 || this.framesTagged !== '0') {
      this.canResetCondition = true;
    } else {
      this.canResetCondition = false;
    }
  }

  mediaNameKeyDown(event: any): boolean {
    if (event.keyCode === 13) {
      this.searchVideos();
    }
    return true;
  }

  mediaNameChange(mediaName: string) {
    this.checkSearchConditionChanged(mediaName);
  }

  framesTaggedChange() {
    this.checkSearchConditionChanged();
  }

  resetCondition() {
    this.mediaName = '';
    this.selectedTags = '';
    this.framesTagged = '0';
    this.selectedTagsText = '全部视频标签';
    const beginTimeInput = $('#beginTimeInput');
    beginTimeInput.clear();
    const endTimeInput = $('#endTimeInput');
    endTimeInput.clear();
    this.canResetCondition = false;
  }

  updateSelectedTags() {
    this.selectedTagsText = this.selectedTags.length > 0 ?
            '已选择 ' + this.selectedTags.split(',').length + ' 个视频标签' : '全部视频标签';
  }
  selectTags() {
    this.isSelectDlgShow = true;
  }
  selectTagsDlgClose() {
    this.isSelectDlgShow = false;
  }
  selectedTagsChange( selectedTags: string ) {
    this.selectedTags = selectedTags;
    this.checkSearchConditionChanged();
    this.updateSelectedTags();
    this.searchHistory.updateVideoHistoryTags(selectedTags.split(','));
    this.isSelectDlgShow = false;
  }

  searchVideos() {
    this.currentPageChanged(1);
  }

  currentPageChanged(currentPage: number) {
    const beginTime = $('#beginTimeInput').val();
    const endTime = $('#endTimeInput').val();

    if (beginTime.length === 0 || endTime.length === 0 || endTime >= beginTime) {
      this.http.searchVideo(currentPage, this.perPageTotalCount, this.mediaName,
          this.selectedTags, beginTime, endTime,
          this.framesTagged === '0' ? null : (this.framesTagged === '1'),
        (data) => {
          this.searchHistory.videoTag = { mediaName: this.mediaName,
                  selectedTags: this.selectedTags,
                  beginTime: beginTime,
                  endTime: endTime,
                  framesTagged: this.framesTagged,
                  currentPage: 1};
          this.searchHistory.saveVideoTagCondition();

          if (data && data.page && data.page.end > 0 && data.videos && (data.videos.length > 0)) {
            this.page = data.page;
            this.videos = data.videos;
            this.searchHistory.videoTag.currentPage = this.page.current;
            this.searchHistory.saveVideoTagCondition();
          } else {
            this.page = null;
            this.videos = [];
          }
        },
        () => {
          this.page = null;
          this.videos = [];
        }
      );
    } else {
      this.isInformationDlgShow = true;
      this.informationDlgText = '开始时间不能大于结束时间！';
    }
  }

  showMoreTags(video: TagSearch.Video) {
    this.overviewVideo = video;
  }

  moreTagsDlgClose() {
    this.overviewVideo = undefined;
  }

  showFrameTags(video: TagSearch.Video) {
    this.searchHistory.frameTag = {
      mediaId: video.mediaId,
      selectedTags: '',
      beginTime: '',
      endTime: ''
    };
    this.searchHistory.saveFrameTagCondition();
    this.router.navigate(['/home/frame-tags-management']);
  }

  updateVideoStatus(index: number) {
    this.http.updateVideoStatus(this.videos[index].mediaId, !this.videos[index].framesTagged,
      () => {
        this.videos[index].framesTagged = !this.videos[index].framesTagged;
      },
      () => {}
    );
  }

  gotoVideoDetail(mediaUrlId: string) {
    window.open('http://www.isherc.com:86/ResourceDetail/ResourceDetails?resId=' + mediaUrlId);
  }

  informationDlgClose() {
    this.isInformationDlgShow = false;
      this.informationDlgText = '';
  }
}
