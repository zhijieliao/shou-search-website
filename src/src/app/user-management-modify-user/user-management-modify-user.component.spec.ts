import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagementModifyUserComponent } from './user-management-modify-user.component';

describe('UserManagementModifyUserComponent', () => {
  let component: UserManagementModifyUserComponent;
  let fixture: ComponentFixture<UserManagementModifyUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserManagementModifyUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserManagementModifyUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
