import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../service/http.service';
import { UserSearch } from '../common/user-search-reply';
import { Validator, validate_username, validate_description } from '../common/validator';

@Component({
  selector: 'mms-user-management-modify-user',
  templateUrl: './user-management-modify-user.component.html',
  styleUrls: ['./user-management-modify-user.component.css']
})
export class UserManagementModifyUserComponent implements OnInit {
  @Input() params: { oldUser: UserSearch.User, newUser: UserSearch.User, ok: () => void };

  validators: Validator[];
  canSave: boolean;

  constructor(public http: HttpService) {
    this.validators = [
      new Validator(
        (): { success: boolean, message: string } => {
          this.params.newUser.username = this.params.newUser.username.trim();
          return validate_username(this.params.newUser.username);
        }
      ),
      new Validator(
        (): { success: boolean, message: string } => {
          this.params.newUser.description = this.params.newUser.description.trim();
          return validate_description(this.params.newUser.description);
        }
      )
    ];
  }

  ngOnInit() {
  }

  usernameValidate(): Validator {
    return this.validators[0];
  }

  descriptionValidate(): Validator {
    return this.validators[1];
  }

  validate() {
    this.canSave = true;
    for (let i = 0; i < this.validators.length; ++i) {
      const result = this.validators[i].validate(true);
      if (!result.success) {
        this.canSave = false;
        alert(result.message);
        break;
      }
    }
  }

  closeDialog() {
    this.http.showModifyUserDialog = false;
  }

  ok() {
    this.validate();
    if (this.canSave) {
      this.closeDialog();

      this.http.modifyUserInfo(
        this.params.oldUser.userId,
        this.params.oldUser.username,
        this.params.newUser.username,
        this.params.newUser.resetPassword,
        this.params.newUser.description,
        () => {
          if (this.params.ok) {
            this.params.ok();
          }

          alert('修改用户信息成功');
        },
        (summary, particulars) => {
          alert(particulars);
        }
      );
    }
  }
  cancel() {
    this.closeDialog();
  }
}
