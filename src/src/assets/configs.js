window.configs = {
    // 基础URL
    baseUrl: 'http://221.237.156.68:8810/media_search',
    // mediaTypeOfVideo
    mediaTypeOfVideo: [ '视频' ],
    // 媒体搜索每页数量
    mediaPerPageNum: 12,
    // 用户信息每页数量
    userInfoPerPageNum: 10,
    // 日志信息每页数量
    logInfoPerPageNum: 10,
    // 视频标签信息每页数量
    videoTagInfoPerPageNum: 15,
    // 标签最大长度
    tagMaxLength: 20,
    // 标签正则表达式
    tagValidRegExp: '^[0-9a-zA-Z\u4e00-\u9FFF]+$',
};
