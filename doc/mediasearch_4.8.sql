CREATE DATABASE  IF NOT EXISTS `mediasearch` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mediasearch`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.1.102    Database: mediasearch
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_log`
--

DROP TABLE IF EXISTS `tb_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log` (
  `id` bigint(38) NOT NULL AUTO_INCREMENT,
  `level` int(1) NOT NULL,
  `operType` int(1) NOT NULL,
  `username` varchar(128) DEFAULT NULL,
  `time` timestamp(6) NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `content` varchar(512) NOT NULL,
  `ipaddr` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log`
--

--
-- Table structure for table `tb_pk_generator`
--

DROP TABLE IF EXISTS `tb_pk_generator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pk_generator` (
  `name` varchar(128) NOT NULL,
  `value` bigint(38) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主键值';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pk_generator`
--

LOCK TABLES `tb_pk_generator` WRITE;
/*!40000 ALTER TABLE `tb_pk_generator` DISABLE KEYS */;
INSERT INTO `tb_pk_generator` VALUES ('tb_user',1);
/*!40000 ALTER TABLE `tb_pk_generator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `id` bigint(38) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '3' COMMENT '用户类型，平台用户：0，系统管理员用户：1，平台用户添加的普通用户：2，系统管理员添加的普通用户：3',
  `account` varchar(128) NOT NULL COMMENT '账号',
  `username` varchar(128) NOT NULL COMMENT '用户名',
  `passwd` varchar(32) NOT NULL COMMENT '密码',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `description` varchar(256) DEFAULT NULL COMMENT '描述',
  `removeTag` int(1) NOT NULL DEFAULT '0' COMMENT '删除标记，已经删除：1，未删除：0',
  `lastLoginTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_UNIQUE` (`account`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (-1,1,'admin','系统管理员','03199f14fb2360f91f59b2024d153f50',now(),'系统管理员',0,null),(0,0,'system','平台用户','03199f14fb2360f91f59b2024d153f50',now(),'平台用户',0,null);
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mediasearch'
--

--
-- Dumping routines for database 'mediasearch'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-29 16:31:06
